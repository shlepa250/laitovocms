<?php

namespace common\models\website;

use Yii;

/**
 * This is the model class for table "{{%menuitem_lang}}".
 */
class MenuitemLang extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menuitem_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menuitem_id' => Yii::t('app', 'Пункт меню'),
            'lang' => Yii::t('app', 'Язык'),
            'name' => Yii::t('app', 'Название'),
            'link' => Yii::t('app', 'Ссылка'),
            'image' => Yii::t('app', 'Изображение'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuitem()
    {
        return $this->hasOne(Menuitem::className(), ['id' => 'menuitem_id']);
    }
}
