<?php

namespace common\models\website;

use Yii;

/**
 * This is the model class for table "{{%bannertype_lang}}".
 */
class BannertypeLang extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bannertype_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bannertype_id' => Yii::t('app', 'Тип баннера'),
            'lang' => Yii::t('app', 'Язык'),
            'name' => Yii::t('app', 'Название'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannertype()
    {
        return $this->hasOne(Bannertype::className(), ['id' => 'bannertype_id']);
    }
}
