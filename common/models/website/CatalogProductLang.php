<?php

namespace common\models\website;

use Yii;

/**
 * This is the model class for table "{{%catalog_product_lang}}".
 */
class CatalogProductLang extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'catalog_product_id' => Yii::t('app', 'Продукт'),
            'lang' => Yii::t('app', 'Язык'),
            'name' => Yii::t('app', 'Название'),
            'image' => Yii::t('app', 'Изображение'),
            'anons' => Yii::t('app', 'Анонс'),
            'content' => Yii::t('app', 'Контент'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProduct()
    {
        return $this->hasOne(CatalogProduct::className(), ['id' => 'catalog_product_id']);
    }
}
