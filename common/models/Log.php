<?php
/**
 * @link http://www.laitovo.ru/
 * @copyright Copyright (c) 2016 Laitovo LLC
 */

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * Модель для таблицы `{{%log}}`.
 * Данная таблица хранит информацию обо всех действиях, совершенных с объектами ActiveRecord.
 * Действия записываются если класс объекта унаследован от [[LogActiveRecord]]
 *
 * @property \yii\db\ActiveQuery $user Пользователь. This property is read-only.
 *
 * @author basyan <basyan@yandex.ru>
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @ignore
     */
    public static function tableName()
    {
        return '{{%log}}';
    }

    /**
     * @ignore
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['user_id'],
                ],
                'value' => function ($event) {
                    $user = Yii::$app->get('user', false);
                    return ($user && !$user->isGuest && !($this->table=='{{%user}}' && $this->operator=='DELETE' && $this->field_id==$user->id)) ? $user->id : null;
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'ip',
                ],
                'value' => function ($event) {
                    return Yii::$app->request->userip;
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'host',
                ],
                'value' => function ($event) {
                    return Yii::$app->request->hostinfo;
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'url',
                ],
                'value' => function ($event) {
                    return Yii::$app->request->url;
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'app',
                ],
                'value' => function ($event) {
                    return Yii::$app->id;
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'module',
                ],
                'value' => function ($event) {
                    return Yii::$app->controller->module->id;
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'controller',
                ],
                'value' => function ($event) {
                    return Yii::$app->controller->id;
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'action',
                ],
                'value' => function ($event) {
                    return Yii::$app->controller->action->id;
                },
            ],
        ];
    }

    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['field_id', 'user_id', 'date'], 'integer'],
            [['log', 'url'], 'string'],
            [['time'], 'number'],
            [['table', 'model', 'ip', 'host', 'app', 'module', 'controller', 'action', 'operator'], 'string', 'max' => 255]
        ];
    }

    /**
     * @ignore
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'table' => Yii::t('app', 'Таблица'),
            'field_id' => Yii::t('app', 'ID Модели'),
            'model' => Yii::t('app', 'Модель'),
            'ip' => Yii::t('app', 'IP'),
            'host' => Yii::t('app', 'HOST'),
            'url' => Yii::t('app', 'URL'),
            'app' => Yii::t('app', 'Приложение'),
            'module' => Yii::t('app', 'Модуль'),
            'controller' => Yii::t('app', 'Контроллер'),
            'action' => Yii::t('app', 'Действие'),
            'operator' => Yii::t('app', 'Оператор'),
            'log' => Yii::t('app', 'Изменение'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'date' => Yii::t('app', 'Дата'),
            'time' => Yii::t('app', 'Затрачено времени'),
        ];
    }

    /**
     * Пользователь совершивший действие
     *
     * @return \yii\db\ActiveQuery Пользователь
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
