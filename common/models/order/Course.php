<?php

namespace common\models\order;

use Yii;

/**
 * This is the model class for table "{{%course}}".
 */
class Course extends \common\models\LogActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_from' => Yii::t('app', 'Валюта 1'),
            'currency_to' => Yii::t('app', 'Валюта 2'),
            'k' => Yii::t('app', 'Коэффициент'),
        ];
    }
}
