<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Menu;

?>
<div class="site-menubar">
    <?
        if (Yii::$app->user->can('admin')) $mainMenuItems[] = [
            'label' => Yii::t('app', 'Администрирование'), 'url' => ['/admin/default/index'],
            'template' => '<a href="{url}"><i class="site-menu-icon fa fa-cogs" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
        ];
        if (Yii::$app->team->can('order')) $mainMenuItems[] = [
            'label' => Yii::t('app', 'Торговля'), 'url' => ['/order/default/index'],
            'template' => '<a href="{url}"><i class="site-menu-icon fa fa-shopping-cart" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
        ];
        if (Yii::$app->team->can('user')) $mainMenuItems[] = [
            'label' => Yii::t('app', 'Контрагенты'), 'url' => ['/user/default/index'],
            'template' => '<a href="{url}"><i class="site-menu-icon fa fa-users" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
        ];
        if (Yii::$app->team->can('website')) $mainMenuItems[] = [
            'label' => Yii::t('app', 'Интернет-магазины'), 'url' => ['/website/default/index'],
            'template' => '<a href="{url}"><i class="site-menu-icon fa fa-shopping-basket" aria-hidden="true"></i><span class="site-menu-title">{label}</span></a>'
        ];
        echo Menu::widget([
            'labelTemplate' => '<a href="javascript:void(0)"><span class="site-menu-title">{label}</span><span class="site-menu-arrow"></span></a>',
            'options' => ['class'=>'site-menu'],
            'itemOptions' =>['class'=>'site-menu-item'],
            'items' => isset($mainMenuItems) ? $mainMenuItems : [],
            'submenuTemplate' => "\n<ul class='site-menu-sub'>\n{items}\n</ul>\n",
            'linkTemplate' => '<a href="{url}"><span class="site-menu-title">{label}</span></a>',
            // 'encodeLabels' => false,
            'activateParents' => true,
        ]);
    ?>

</div>
