<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
if (isset($this->params['searchmodel'])):
?>

    <?php $form = ActiveForm::begin([
        'action' => isset($this->params['searchmodelaction']) ? $this->params['searchmodelaction'] : ['index'],
        'method' => 'get',
    ]); ?>

    <?if (Yii::$app->request->get('search')):?><a href="<?=Url::to(isset($this->params['searchmodelaction']) ? $this->params['searchmodelaction'] : ['index'])?>" class="pull-right btn btn-icon btn-default btn-outline btn-round" data-toggle="tooltip" data-original-title="<?=Yii::t('app', 'Сбросить')?>"><i class="icon wb-refresh" aria-hidden="true"></i></a> <?endif?>
    <div class="input-search input-search-dark pull-right"><i class="input-search-icon wb-search" aria-hidden="true"></i><input type="text" class="form-control" value="<?=Yii::$app->request->get('search')?>" name="search" placeholder="<?=Yii::t('app', 'Поиск')?>"></div>

    <?php ActiveForm::end(); ?>

<?endif;