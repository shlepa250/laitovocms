<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

?>
<?php $this->beginContent('@backend/themes/remark/layouts/main.php'); ?>
<!-- Page -->
<div class="page animsition">
    <div class="page-aside page-aside-fixed">
        <div class="page-aside-switch">
            <i class="icon wb-chevron-left" aria-hidden="true"></i>
            <i class="icon wb-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner" data-plugin="pageAsideScroll">
            <div data-role="container">
                <div data-role="content">
                    <?
                        if (isset($this->params['menuItems'])) {
                            foreach ($this->params['menuItems'] as $key => $value) {
                                $this->params['menuItems'][$key]['options']=['tag'=>'section','class'=>'page-aside-section'];
                            }
                        }

                        echo Menu::widget([
                            'labelTemplate' => '<h5 class="page-aside-title">{label}</h5>',
                            'options' => ['tag'=>false],
                            'itemOptions' =>['tag'=>'div'],
                            'items' => isset($this->params['menuItems']) ? $this->params['menuItems'] : [],
                            'submenuTemplate' => "\n<div class='list-group'>\n{items}\n</div>\n",
                            'linkTemplate' => '<a class="list-group-item" href="{url}">{label}</a>',
                            'encodeLabels' => false,
                        ]);
                    ?>

                </div>
            </div>
        </div>
    </div>
    <div class="page-main">
        <div class="page-header">
            <h1 class="page-title"><?=Html::encode($this->title)?></h1>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="page-header-actions">
                <?php $this->beginContent('@backend/themes/remark/layouts/_search.php'); ?>
                <?php $this->endContent(); ?>
            </div>
        </div>
        <div class="page-content">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title"><?=Html::encode($this->title)?></h3>
                </div>
                <div class="panel-body">
                    <?=$content?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Page -->
<?php $this->endContent(); ?>
