<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\themes\remark\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BreakpointsAsset extends AssetBundle
{
    // public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $sourcePath = '@backend/themes/remark/assets/app';
    public $js = [
        'breakpoints.js-0.4.2/breakpoints.min.js',
        'js/breakpoints.js',
    ];
    public $depends = [
        'xj\modernizr\ModernizrAsset',
    ];

}
