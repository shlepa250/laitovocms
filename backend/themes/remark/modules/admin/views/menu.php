<?php

/* @var $this yii\web\View */

$this->params['menuItems'] = [
    [
        'items' => [
            ['label' => Yii::t('app', 'Пользователи'), 'url' => ['user/index']],
            ['label' => Yii::t('app', 'Команды'), 'url' => ['team/index']],
            ['label' => Yii::t('app', 'Логи'), 'url' => ['log/index']],
        ],
    ],
];