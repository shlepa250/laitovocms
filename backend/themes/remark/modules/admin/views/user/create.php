<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\user\User */

$this->title = Yii::t('app', 'Добавить пользователя');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Администрирование'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['searchmodel'] = true;
$this->render('../menu');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

