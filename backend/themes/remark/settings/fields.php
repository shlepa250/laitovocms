<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\SettingsForm */

use yii\helpers\Html;
use yii\helpers\Json;
use yii\bootstrap\ActiveForm;
use backend\widgets\formbuilder\BuilderAsset;

BuilderAsset::register($this);

Yii::$app->layout='2columns';
$this->render('menu');

$this->title = Yii::t('app', 'Конструктор форм. '.$field.'.');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки'), 'url' => ['/settings']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Команда: {team}',['team'=>$model->team->name]), 'url' => ['team','id'=>$model->team->id]];
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->view->registerJs('

    $(function() {
        var offset = $("#fixedtabbable").offset();
        var topPadding = 60;
        $(window).scroll(function() {
            if ($(window).scrollTop() > offset.top) {
                if (($("#build").height()-offset.top-topPadding)-$(window).scrollTop()>0)
                    $("#fixedtabbable").stop().animate({marginTop: $(window).scrollTop() - offset.top + topPadding});
            }
            else {$("#fixedtabbable").stop().animate({marginTop: 0});};
        });
    });

');

?>
<script type="text/javascript">
    var FormBuilderTarget='<?=$model->target?>';
</script>
<div class="row">
    <!-- Building Form. -->
    <div class="col-md-6">
        <div class="clearfix">
            <div id="build">
                <form id="target">
                </form>
            </div>
        </div>
    </div>
    <!-- / Building Form. -->

    <!-- Components -->
    <div class="col-md-6">
        <div class="tabbable" id="fixedtabbable">
            <ul class="nav nav-tabs" id="formtabs">
            <!-- Tab nav -->
            </ul>
            <form id="components">
                <fieldset>
                    <div class="tab-content">
                      <!-- Tabs of snippets go here -->
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <!-- / Components -->

</div>


<?php $form = ActiveForm::begin(['id'=>'teamfields-form']); ?>

<?= $form->field($model, 'target')->hiddenInput()->label(false) ?>
<?= $form->field($model, 'render')->hiddenInput()->label(false) ?>

<?php ActiveForm::end(); ?>

<div class="form-group">
	<?= Html::submitButton( Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary btn-round btn-outline','onclick'=>'$("#teamfields-form").submit();']) ?>
</div>
