<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\SettingsForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

Yii::$app->layout='2columns';
$this->render('menu');

$this->title = Yii::t('app', 'Профиль');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки'), 'url' => ['/settings']];
$this->params['breadcrumbs'][] = $this->title;

if ($model->hasErrors('password'))
	Yii::$app->view->registerJs("$('.field-settingsform-password').show();");

Yii::$app->view->registerJs("
	$('input[name=\"SettingsForm[email]\"]').keyup(function(){
		if ($(this).val()!='".$model->user->email."') $('.field-settingsform-password').show(); else $('.field-settingsform-password').hide();
	})
");
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'password',['options' => ['class'=>'form-group', 'style'=>'display:none;']])->passwordInput(['maxlength' => true]) ?>

<?= Html::submitButton( Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary btn-round btn-outline']) ?>

<?php ActiveForm::end(); ?>
