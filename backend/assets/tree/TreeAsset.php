<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets\tree;

use yii\web\AssetBundle;

/**
 * @author basyan <basyan@yandex.ru>
 */
class TreeAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/tree/assets';
    public $css = [
        'bootstrap-treeview.min.css',
        'treeview.custom.css',
    ];
    public $js = [
        'bootstrap-treeview.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
