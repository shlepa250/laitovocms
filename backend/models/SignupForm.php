<?php
namespace backend\models;

use Yii;
use common\models\User;
use common\models\team\Team;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $team;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'string', 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 
                'targetClass' => '\common\models\User',
                'filter' => ['website_id' => Yii::$app->params['website_id']],
                'message' => Yii::t('app', 'Данный адрес электронной почты уже зарегистрирован.')
            ],

            ['team', 'required', 'when' => function($model) {
                return Yii::$app->session['invite_team_id'] === null;
            }],
            ['team', 'string', 'max' => 255],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Ваше имя'),
            'team' => Yii::t('app', 'Название команды'),
            'email' => Yii::t('app', 'Ваш E-mail'),
            'password' => Yii::t('app', 'Придумайте пароль'),
        ];
    }

    public function __construct($config = [])
    {
        if (Yii::$app->session['invite_email'])
            $this->email=Yii::$app->session['invite_email'];
        parent::__construct($config);
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->name = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                if (Yii::$app->session['invite_team_id']){
                    $team=Team::findOne(Yii::$app->session['invite_team_id']);
                    if ($team) {
                        $json=Json::decode($team->json ? $team->json : '{}');
                        if (Yii::$app->session['invite_email'] && isset($json['Invite'][Yii::$app->session['invite_email']])) {
                            unset($json['Invite'][Yii::$app->session['invite_email']]);
                            $team->json=Json::encode($json);
                            $team->save();
                            $team->link('users', $user);
                            Yii::$app->team->setTeam($team->id);
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Добро пожаловать в команду "{team}".',['team'=>$team->name]));
                        }
                    }
                } else {
                    $team=new Team;
                    $team->name=$this->team;
                    if ($team->save()){
                        $team->link('users', $user);
                        $team->link('author', $user);
                        Yii::$app->team->setTeam($team->id);
                    }
                }
                return $user;
            }
        }

        return null;
    }
}
