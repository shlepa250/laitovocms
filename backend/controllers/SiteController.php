<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use backend\models\SignupForm;
use backend\models\ResetPasswordForm;
use backend\models\PasswordResetRequestForm;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use common\models\User;
use common\models\team\Team;
use yii\helpers\Json;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['error', 'captcha'],
                'rules' => [
                    [
                        'actions' => ['login', 'signup', 'request-password-reset', 'reset-password', 'invite'],
                        'roles' => ['?'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user, 3600 * 24 * 3000)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Проверьте вашу электронную почту для получения дальнейших инструкций.'));

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Восстановление пароля невозможно.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Пароль изменен.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionInvite($team, $email, $invite)
    {
        $team=(int)$team;
        
        if (empty($team) || empty($email) || empty($invite) || !is_int($team) || !is_string($email) || !is_string($invite) || ($_team=Team::findOne($team))===null )
            throw new BadRequestHttpException(Yii::t('app', 'Неверная ссылка.'));

        $json=Json::decode($_team->json ? $_team->json : '{}');
        if (isset($json['Invite'][$email]) && $json['Invite'][$email]==$invite) {
            $user=User::findByEmail($email);
            if ($user) {
                if (!Yii::$app->user->isGuest && Yii::$app->user->id!=$user->id)
                    Yii::$app->user->logout();
                $_team->unlink('users', $user, true);
                $_team->link('users', $user);
                unset($json['Invite'][$email]);
                $_team->json=Json::encode($json);
                $_team->save();
                Yii::$app->team->setTeam($_team->id);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Добро пожаловать в команду "{team}".',['team'=>$_team->name]));
                return $this->goHome();
            } else {
                Yii::$app->user->logout();
                Yii::$app->session['invite_team_id']=$_team->id;
                Yii::$app->session['invite_email']=$email;
                return $this->redirect(['signup']);
            }
        } else {
            throw new BadRequestHttpException(Yii::t('app', 'Неверная ссылка.'));
        }
    }

    public function actionSetTeam($id)
    {
        Yii::$app->team->setTeam($id);
        return $this->redirect(['settings/team','id'=>Yii::$app->team->id]);
    }
}
