<?php

namespace backend\controllers;

use Yii;
use common\models\Region;
use common\models\order\Price;
use common\models\team\Team;
use common\models\User;
use common\models\team\Organization;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {            
        if (!Yii::$app->request->isAjax)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        return parent::beforeAction($action);
    }

    public function actionOrganizations($team)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $team=Team::find()->where(['id'=>$team])->one();
        if ($team)
            return ArrayHelper::map($team->getOrganizations()->asArray()->all(),'id','name');
        
        return [];
    }

    public function actionTeam($team)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $team=Team::find()->where(['id'=>$team])->one();
        if ($team){
            $json=Json::decode($team->json ? $team->json : '{}');

            if (isset($json['country']) && ($region=Region::find()->where(['id'=>$json['country']])->one())!==null)
                $regions = ArrayHelper::map($region->getRegions()->asArray()->all(),'id','name');

            if (isset($json['region']) && ($region=Region::find()->where(['id'=>$json['region']])->one())!==null)
                $citys = ArrayHelper::map($region->getRegions()->asArray()->all(),'id','name');

            return [
                'country'=>isset($json['country']) ? $json['country'] : null,
                'region'=>isset($json['region']) ? $json['region'] : null,
                'city'=>isset($json['city']) ? $json['city'] : null,
                'regions'=>isset($regions) ? $regions : null,
                'citys'=>isset($citys) ? $citys : null,
            ];
        }
        
        return [];
    }

    public function actionRegions($region)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $region=(int)$region;

        $region=Region::find()->where(['id'=>$region])->one();
        if ($region)
            return ArrayHelper::map($region->getRegions()->asArray()->all(),'id','name');
        
        return [];
    }

    public function actionPrice($product, $currency=null, $country, $region, $city,$team=null,$user=null,$discount=0)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $product=(int)$product;
        $country=(int)$country;
        $region=(int)$region;
        $city=(int)$city;
        $team=(int)$team;
        $user=(int)$user;

        $team=Team::find()->where(['id'=>$team])->one();
        $user=User::find()->where(['id'=>$user])->one();
        $price=Price::find()->where(['region_id'=>$city,'product_id'=>$product])->one();
        if (!$price)
            $price=Price::find()->where(['region_id'=>$region,'product_id'=>$product])->one();
        if (!$price)
            $price=Price::find()->where(['region_id'=>$country,'product_id'=>$product])->one();
        if ($price){
            $_price=$price->price(true, $currency)-$price->price(true, $currency)*$discount/100;

            if ($team && $team->discount){
                $_price=$_price-$_price*$team->discount/100;
            } elseif ($user && $user->discount){
                $_price=$_price-$_price*$user->discount/100;
            }
            if (is_numeric($price->maxdiscount)){
                $minprice=$price->price(true, $currency)-$price->price(true, $currency)*$price->maxdiscount/100;
                $_price=$_price<$minprice ? $minprice : $_price;
            }

            return [
                'price'=>$_price,
                'currency'=>$currency,
                'original_price'=>$price->price,
                'original_currency'=>$price->currency,
            ];
        }
        
        return [
            'price'=>0,
        ];
    }

}
