<?php

namespace backend\modules\website\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\website\Feed;
use common\models\website\News;
use yii\web\NotFoundHttpException;

/**
 * Feed
 */
class NewsForm extends Model
{

    public $name;

    public $news;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название'),
        ];
    }

    public function __construct($news_id=null, $feed_id=null, $config = [])
    {
        if ($news_id===null && Feed::find()->joinWith('website')->where(['{{%feed}}.id'=>$feed_id,'{{%website}}.team_id'=>Yii::$app->team->id])->one()!==null ) {

            $this->news=new News;
            $this->news->feed_id=$feed_id;

        } elseif ($news_id && ($this->news=News::find()->joinWith('feed.website')->where(['{{%news}}.id'=>$news_id,'{{%website}}.team_id'=>Yii::$app->team->id])->one())!==null ) {

            $this->name=$this->news->name;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $news = $this->news;

            $news->name=$this->name;

            return $news->save();
        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->news->delete();
    }
}
