<?php

namespace backend\modules\order;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\order\controllers';

    public function init()
    {
		\Yii::$app->layout='2columns';
        parent::init();

        // custom initialization code goes here
    }
}
