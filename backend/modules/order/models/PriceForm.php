<?php

namespace backend\modules\order\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\Region;
use common\models\order\Product;
use common\models\order\Price;
use yii\web\NotFoundHttpException;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\RequiredValidator;

/**
 * price
 */
class PriceForm extends Model
{

    public $price;
    public $oldprice;
    public $currency;
    public $maxdiscount;
    public $nds;
    public $nds_in=1;
    public $cur=[
        'RUB'=>'RUB',
        'EUR'=>'EUR',
        'USD'=>'USD',
        'KZT'=>'KZT',
    ];

    public $_price;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price','currency'], 'required'],
            [['price','oldprice','nds'], 'number', 'min'=>0],
            [['maxdiscount'], 'number', 'min'=>0, 'max'=>100],
            [['nds_in'], 'boolean'],
            [['currency'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'price' => Yii::t('app', 'Цена'),
            'oldprice' => Yii::t('app', 'Старая цена'),
            'nds' => Yii::t('app', 'Ставка НДС (%)'),
            'maxdiscount' => Yii::t('app', 'Максимальная скидка (%)'),
            'nds_in' => Yii::t('app', 'НДС включен в цену'),
            'currency' => Yii::t('app', 'Валюта'),
        ];
    }

    public function __construct($region_id, $product_id, $config = [])
    {
        if (($this->_price = Price::find()->joinWith('product')->where(['team_id'=>Yii::$app->team->id,'region_id' => $region_id, 'product_id' => $product_id])->one()) !== null) {

            $this->price=$this->_price->price;
            $this->oldprice=$this->_price->oldprice;
            $this->maxdiscount=$this->_price->maxdiscount;
            $this->nds=$this->_price->nds;
            $this->nds_in=$this->_price->nds_in;
            $this->currency=$this->_price->currency;

        } elseif (Region::find()->where(['id' => $region_id])->one() !== null && Product::find()->where(['id'=>$product_id,'team_id'=>Yii::$app->team->id])->one()!==null ) {

            $this->_price=new Price();
            $this->_price->region_id=$region_id;
            $this->_price->product_id=$product_id;

        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $price = $this->_price;

            $price->price=$this->price;
            $price->oldprice=$this->oldprice;
            $price->maxdiscount=$this->maxdiscount;
            $price->nds=$this->nds;
            $price->nds_in=$this->nds_in;
            $price->currency=$this->currency;

            return $price->save();
        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->_price->delete();
    }
}
