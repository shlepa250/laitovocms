<?php

namespace backend\modules\order\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\order\Category;
use yii\web\NotFoundHttpException;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\RequiredValidator;
use yii\helpers\ArrayHelper;

/**
 * Category
 */
class CategoryForm extends Model
{

    public $name;
    public $sort=100;
    public $parent_id;
    public $fields=[];

    public $category;
    private $_json=[];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['parent_id','sort'], 'number'],
            [['parent_id'], 'validateParentid'],
            [['fields'], 'each','rule' => ['string']],
        ];
    }

    /**
     * Validates the parent_id.
     */
    public function validateParentid($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->validateSubItems($this->category, $this->parent_id, $attribute, $params);
        }
    }
    private function validateSubItems($category, $parent_id, $attribute, $params)
    {
        if ($parent_id==$category->id) {
            $this->addError($attribute, Yii::t('yii', '{attribute} is invalid.', ['attribute' => $this->getAttributeLabel($attribute)]));
        } else {
            if ($category->categories){
                foreach ($category->categories as $row) {
                    $this->validateSubItems($row, $parent_id, $attribute, $params);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
            'parent_id' => Yii::t('app', 'Родитель'),
            'sort' => Yii::t('app', 'Сортировка'),
        ];
    }

    public function __construct($category_id=null, $config = [])
    {
        if ($category_id===null){

            $this->category=new Category;
            $this->category->team_id=Yii::$app->team->id;

            foreach ($this->category->fields as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=null;
            }

        } elseif ( ($this->category=Category::find()->where(['id'=>$category_id,'team_id'=>Yii::$app->team->id])->one())!==null ) {

            $this->_json=Json::decode($this->category->json ? $this->category->json : '{}');

            $this->name=$this->category->name;
            $this->sort=$this->category->sort;
            $this->parent_id=$this->category->parent_id;

            foreach ($this->category->fields as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=isset($this->_json[$value['fields']['id']['value']]) ? $this->_json[$value['fields']['id']['value']] : '';
            }

        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $json = $this->_json;

            $category = $this->category;

            $category->name=$this->name;
            $category->sort=$this->sort;
            $category->parent_id=$this->parent_id;

            foreach ($category->fields as $key => $value) {
                $json[$value['fields']['id']['value']]=isset($this->fields[$value['fields']['id']['value']]) ? $this->fields[$value['fields']['id']['value']] : '';
            }
            $category->json=Json::encode($json);

            return $category->save();
        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->category->delete();
    }

    public function getItems()
    {
        $items=[];
        $_items=$this->category->team->getCategories()->andWhere(['parent_id'=>null])->orderBy('sort')->all();
        foreach ($_items as $row) {
            $items=ArrayHelper::merge($items, $this->getSubItems($row));
        }
        return $items;
    }

    private function getSubItems($item, $i='')
    {
        $result[] =[
            'name' => $i.' '.$item->name,
            'id' => $item->id,
        ];
        $items=$item->getCategories()->orderBy('sort')->all();
        if ($items){
            $i.=' -';
            foreach ($items as $row) {
                $result=ArrayHelper::merge($result, $this->getSubItems($row, $i));
            }
        }
        return $result;
    }


}
