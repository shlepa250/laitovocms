<?php

namespace backend\modules\order\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\order\Product;
use common\models\order\Price;
use common\models\Region;
use common\models\order\Category;
use yii\web\NotFoundHttpException;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\RequiredValidator;
use yii\helpers\ArrayHelper;

/**
 * Product
 */
class ProductForm extends Model
{

    public $name;
    public $article;
    public $unit;
    public $quantity;
    public $maxdiscount;
    public $units=['шт','кг','м','м²','м³','пог. м','час'];
    public $categories='[]';

    public $price;
    public $oldprice;
    public $currency;
    public $nds;
    public $nds_in=0;
    public $cur=[
        'RUB'=>'RUB',
        'EUR'=>'EUR',
        'USD'=>'USD',
        'KZT'=>'KZT',
    ];

    public $fields=[];

    public $product;
    private $_price;
    private $_json=[];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price','oldprice','nds'], 'number', 'min'=>0],
            [['nds_in'], 'boolean'],
            [['currency'], 'string'],

            [['name'], 'required'],
            [['categories'], 'string'],
            ['categories', 'default', 'value' => '[]'],
            [['quantity'], 'number'],
            [['maxdiscount'], 'number', 'min'=>0, 'max'=>100],
            [['name', 'article', 'unit'], 'string', 'max' => 255],
            ['article', 'unique', 
                'targetClass' => '\common\models\order\Product',
                'filter' => ['and', ['team_id' => Yii::$app->team->id], ['not',['id' => $this->product->id]]],
            ],
            [['categories'], 'validateCategory'],
            [['fields'], 'each','rule' => ['string']],
        ];
    }

    public function validateCategory($attribute, $params)
    {
        $number = new NumberValidator();

        if (!$this->hasErrors()) {
            if ($this->categories){
                try {
                    $categories=Json::decode($this->categories);
                    if ($categories){
                        foreach ($categories as $row) {
                            if ($number->validate($row, $error)) {
                            } else {
                                $this->addError($attribute, Yii::t('yii', 'Invalid data received for parameter "{param}".', ['param'=>$this->getAttributeLabel($attribute)]));
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $this->addError($attribute, Yii::t('yii', 'Invalid data received for parameter "{param}".', ['param'=>$this->getAttributeLabel($attribute)]));
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
            'article' => Yii::t('app', 'Артикул'),
            'quantity' => Yii::t('app', 'Кол-во (остаток)'),
            'maxdiscount' => Yii::t('app', 'Максимальная скидка (%)'),
            'unit' => Yii::t('app', 'Ед. измерения'),
            'categories' => Yii::t('app', 'Категории'),

            'price' => Yii::t('app', 'Цена'),
            'oldprice' => Yii::t('app', 'Старая цена'),
            'nds' => Yii::t('app', 'Ставка НДС (%)'),
            'nds_in' => Yii::t('app', 'НДС включен в цену'),
            'currency' => Yii::t('app', 'Валюта'),
        ];
    }

    public function __construct($product_id=null, $region_id=1, $config = [])
    {
        if ($product_id===null){

            $this->product=new Product;
            $this->product->team_id=Yii::$app->team->id;
            $this->_price=new Price();
            $this->_price->region_id=$region_id;

            foreach ($this->product->fields as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=null;
            }

        } elseif ( ($this->product=Product::find()->where(['id'=>$product_id,'team_id'=>Yii::$app->team->id])->one())!==null ) {

            if (($this->_price = Price::find()->where(['region_id' => $region_id, 'product_id' => $product_id])->one()) !== null) {

                $this->price=$this->_price->price;
                $this->oldprice=$this->_price->oldprice;
                $this->maxdiscount=$this->_price->maxdiscount;
                $this->nds=$this->_price->nds;
                $this->nds_in=$this->_price->nds_in;
                $this->currency=$this->_price->currency;

            } elseif (Region::find()->where(['id' => $region_id])->one() !== null ) {

                $this->_price=new Price();
                $this->_price->region_id=$region_id;
                $this->_price->product_id=$product_id;

            }

            $this->_json=Json::decode($this->product->json ? $this->product->json : '{}');

            $this->name=$this->product->name;
            $this->article=$this->product->article;
            $this->quantity=$this->product->quantity;
            $this->unit=$this->product->unit;

            foreach ($this->product->fields as $key => $value) {
                $this->fields[$value['fields']['id']['value']]=isset($this->_json[$value['fields']['id']['value']]) ? $this->_json[$value['fields']['id']['value']] : '';
            }

            $categories=[];
            if ($this->product->categories){
                foreach ($this->product->categories as $cat) {
                    $categories[]=$cat->id;
                }
            }
            $this->categories=Json::encode($categories);
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }

    public function save()
    {
        if ($this->validate()) {
            $product = $this->product;

            $json = $this->_json;

            $product->name=$this->name;
            $product->article=$this->article;
            $product->quantity=$this->quantity;
            $product->unit=$this->unit;

            foreach ($product->fields as $key => $value) {
                $json[$value['fields']['id']['value']]=isset($this->fields[$value['fields']['id']['value']]) ? $this->fields[$value['fields']['id']['value']] : '';
            }
            $product->json=Json::encode($json);

            if ($product->save()){

                $price = $this->_price;
                $price->product_id=$product->id;
                $price->price=$this->price;
                $price->oldprice=$this->oldprice;
                $price->maxdiscount=$this->maxdiscount;
                $price->nds=$this->nds;
                $price->nds_in=$this->nds_in;
                $price->currency=$this->currency;
                if (!is_numeric($price->price) && !is_numeric($price->oldprice) && !$price->isNewRecord)
                    $price->delete();
                elseif (is_numeric($price->price) || is_numeric($price->oldprice))
                    $price->save();


                foreach ($product->categories as $category) {
                    $product->unlink('categories', $category, true);
                }

                $categories=Json::decode($this->categories);
                foreach ($categories as $cat) {
                    $category=Category::find()->where(['id'=>$cat,'team_id'=>Yii::$app->team->id])->one();
                    if ($category)
                        $product->link('categories', $category);
                }
                return true;
            }
        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->product->delete();
    }
}
