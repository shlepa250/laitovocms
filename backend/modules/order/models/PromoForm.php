<?php

namespace backend\modules\order\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\Region;
use common\models\order\Promo;
use yii\web\NotFoundHttpException;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\RequiredValidator;

/**
 * promo
 */
class PromoForm extends Model
{

    public $name;

    public $promo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Название'),
        ];
    }

    public function __construct($promo_id=null, $config = [])
    {
        if ($promo_id===null){

            $this->promo=new Promo;
            $this->promo->team_id=Yii::$app->team->id;

        } elseif ( ($this->promo=Promo::find()->where(['id'=>$promo_id,'team_id'=>Yii::$app->team->id])->one())!==null ) {

            $this->name=$this->promo->name;

        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        parent::__construct($config);
    }


    public function save()
    {
        if ($this->validate()) {
            $promo = $this->promo;

            $promo->name=$this->name;

            return $promo->save();
        } else {
            return false;
        }
    }

    public function delete()
    {
        return $this->promo->delete();
    }
}
