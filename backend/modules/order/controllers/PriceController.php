<?php

namespace backend\modules\order\controllers;

use Yii;
use common\models\order\Product;
use backend\modules\order\models\PriceForm;
use common\models\Region;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PriceController implements the CRUD actions for Price model.
 */
class PriceController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->team->can('order');
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $query = Region::find()->where(['status'=>1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'name', Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'query' => $query,
        ]);
    }

    public function actionView($id)
    {

        $model = Region::findOne($id);
        if ($model===null)
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        
        $query = Product::find()->with('prices')->where(['team_id'=>Yii::$app->team->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $validator = new \yii\validators\StringValidator();

        if ($validator->validate(Yii::$app->request->get('search'), $error)) {

            $query->andFilterWhere(['or', 
                ['like', 'name', Yii::$app->request->get('search')],
                ['like', 'article', Yii::$app->request->get('search')],
            ]);
        }

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Price model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $region_id
     * @param integer $product_id
     * @return mixed
     */
    public function actionUpdate($region_id, $product_id)
    {
        $model = new PriceForm($region_id, $product_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $region_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Price model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $region_id
     * @param integer $product_id
     * @return mixed
     */
    public function actionDelete($region_id, $product_id)
    {
        $model = new PriceForm($region_id, $product_id);
        $model->delete();

        return $this->redirect(['view','id'=>$region_id]);
    }
}
