<?php

namespace app\modules\laitovo\controllers;

use yii\web\Controller;

/**
 * Default controller for the `laitovo` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
