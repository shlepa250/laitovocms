<?php
namespace console\controllers;

use Yii;
use yii\httpclient\Client;
use obmen\models\ObmenAfilter;

class ObmenController extends \yii\console\Controller
{
    private $path='obmen/files';

    public function actionOrders($team, $last=0, $upload=true, $filename='orders_')
    {

        $client = new Client();
        $response = $client->createRequest()
            ->addHeaders(['Accept' => 'application/xml'])
            ->setMethod('get')
            ->setUrl(Yii::$app->params['apiurl'].'/order')
            ->setData([
                'access-token' => Yii::$app->params['access_token'],
                'team' => $team,
                'last' => $last,
                'expand' => 'items,clientTeam,clientOrganization',
            ])
            ->send();
        if ($response->isOk) {
            file_put_contents(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$filename.$team.'.xml', $response->content);

            if ($upload) {
                $ftp = new \yii2mod\ftp\FtpClient();
                $ftp->connect(Yii::$app->params['obmen_ftp']['host'], false, Yii::$app->params['obmen_ftp']['port'])
                ->login(Yii::$app->params['obmen_ftp']['login'], Yii::$app->params['obmen_ftp']['password'])
                ->pasv(true);
                
                $ftp->putFromPath(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$filename.$team.'.xml');
            }
        }

    }


    public function actionClients($team, $last=0, $upload=true, $filename='clients_')
    {

        $client = new Client();
        $response = $client->createRequest()
            ->addHeaders(['Accept' => 'application/xml'])
            ->setMethod('get')
            ->setUrl(Yii::$app->params['apiurl'].'/client')
            ->setData([
                'access-token' => Yii::$app->params['access_token'],
                'team' => $team,
                'last' => $last,
                'expand' => 'organizations',
            ])
            ->send();
        if ($response->isOk) {
            file_put_contents(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$filename.$team.'.xml', $response->content);

            if ($upload) {
                $ftp = new \yii2mod\ftp\FtpClient();
                $ftp->connect(Yii::$app->params['obmen_ftp']['host'], false, Yii::$app->params['obmen_ftp']['port'])
                ->login(Yii::$app->params['obmen_ftp']['login'], Yii::$app->params['obmen_ftp']['password'])
                ->pasv(true);
                
                $ftp->putFromPath(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$filename.$team.'.xml');
            }
        }

    }


    public function actionProducts($team, $last=0, $upload=true, $filename='products_')
    {

        $client = new Client();
        $response = $client->createRequest()
            ->addHeaders(['Accept' => 'application/xml'])
            ->setMethod('get')
            ->setUrl(Yii::$app->params['apiurl'].'/product')
            ->setData([
                'access-token' => Yii::$app->params['access_token'],
                'team' => $team,
                'last' => $last,
                'expand' => 'prices,categories',
            ])
            ->send();
        if ($response->isOk) {
            file_put_contents(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$filename.$team.'.xml', $response->content);

            if ($upload) {
                $ftp = new \yii2mod\ftp\FtpClient();
                $ftp->connect(Yii::$app->params['obmen_ftp']['host'], false, Yii::$app->params['obmen_ftp']['port'])
                ->login(Yii::$app->params['obmen_ftp']['login'], Yii::$app->params['obmen_ftp']['password'])
                ->pasv(true);
                
                $ftp->putFromPath(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$filename.$team.'.xml');
            }
        }

    }


    public function actionCategories($team, $last=0, $upload=true, $filename='categories_')
    {

        $client = new Client();
        $response = $client->createRequest()
            ->addHeaders(['Accept' => 'application/xml'])
            ->setMethod('get')
            ->setUrl(Yii::$app->params['apiurl'].'/category')
            ->setData([
                'access-token' => Yii::$app->params['access_token'],
                'team' => $team,
                // 'expand' => '',
            ])
            ->send();
        if ($response->isOk) {
            file_put_contents(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$filename.$team.'.xml', $response->content);

            if ($upload) {
                $ftp = new \yii2mod\ftp\FtpClient();
                $ftp->connect(Yii::$app->params['obmen_ftp']['host'], false, Yii::$app->params['obmen_ftp']['port'])
                ->login(Yii::$app->params['obmen_ftp']['login'], Yii::$app->params['obmen_ftp']['password'])
                ->pasv(true);
                
                $ftp->putFromPath(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$filename.$team.'.xml');
            }
        }

    }

    /**
     * Загрузка контрагентов из 1С ООО "Торговый дом" и ООО "Автофильтр"
     * @return type
     */
    public function actionLoadClientsA($download=true, $files='DataTDK.xml,DataAFK.xml')
    {
        $files=explode(',', $files);

        if ($download){
            $ftp = new \yii2mod\ftp\FtpClient();
            $ftp->connect(Yii::$app->params['obmen_ftp']['host'], false, Yii::$app->params['obmen_ftp']['port'])
            ->login(Yii::$app->params['obmen_ftp']['login'], Yii::$app->params['obmen_ftp']['password'])
            ->pasv(true);

            foreach ($files as $file) {
                @$ftp->get(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$file, $file, FTP_ASCII);
            }
        }

        foreach ($files as $filename) {
            $client = new Client();
            $response = $client->createRequest()
                ->addHeaders(['Accept' => 'application/xml'])
                ->setMethod('get')
                ->setUrl(Yii::$app->params['apiurl'].'/load')
                ->setData([
                    'access-token' => Yii::$app->params['access_token'],
                    'file' => $filename,
                ])
                ->send();
            if ($response->isOk) {
                $obmen=new ObmenAfilter($response->data, $filename);

                try {
                    $obmen->importclients();
                } catch (\Exception $e) {
                    Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['supportEmail'])
                    ->setTo('web@laitovo.ru')
                    ->setSubject( Yii::t('app', 'Проблема синхронизации') )
                    ->setTextBody((string)$e)
                    ->send();
                }
            }
        }
    }

    /**
     * Загрузка номенклатуры из 1С ООО "Автофильтр"
     * @return type
     */
    public function actionLoadProductsA($download=true, $files='DataAFT.xml')
    {
        $files=explode(',', $files);

        if ($download){
            $ftp = new \yii2mod\ftp\FtpClient();
            $ftp->connect(Yii::$app->params['obmen_ftp']['host'], false, Yii::$app->params['obmen_ftp']['port'])
            ->login(Yii::$app->params['obmen_ftp']['login'], Yii::$app->params['obmen_ftp']['password'])
            ->pasv(true);

            foreach ($files as $file) {
                @$ftp->get(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$file, $file, FTP_ASCII);
            }
        }

        foreach ($files as $filename) {
            $client = new Client();
            $response = $client->createRequest()
                ->addHeaders(['Accept' => 'application/xml'])
                ->setMethod('get')
                ->setUrl(Yii::$app->params['apiurl'].'/load')
                ->setData([
                    'access-token' => Yii::$app->params['access_token'],
                    'file' => $filename,
                ])
                ->send();
            if ($response->isOk) {
                $obmen=new ObmenAfilter($response->data, $filename);

                try {
                    $obmen->importproducts();
                } catch (\Exception $e) {
                    Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['supportEmail'])
                    ->setTo('web@laitovo.ru')
                    ->setSubject( Yii::t('app', 'Проблема синхронизации') )
                    ->setTextBody((string)$e)
                    ->send();
                }
            }
        }
    }

    /**
     * Загрузка заказов из 1С ООО "Торговый дом" и ООО "Автофильтр"
     * @return type
     */
    public function actionLoadOrdersA($download=true, $files='DataAFS.xml,DataTDS.xml')
    {
        $files=explode(',', $files);

        if ($download){
            $ftp = new \yii2mod\ftp\FtpClient();
            $ftp->connect(Yii::$app->params['obmen_ftp']['host'], false, Yii::$app->params['obmen_ftp']['port'])
            ->login(Yii::$app->params['obmen_ftp']['login'], Yii::$app->params['obmen_ftp']['password'])
            ->pasv(true);

            foreach ($files as $file) {
                @$ftp->get(dirname(Yii::$app->basePath).'/'.$this->path.'/'.$file, $file, FTP_ASCII);
            }
        }

        foreach ($files as $filename) {
            $client = new Client();
            $response = $client->createRequest()
                ->addHeaders(['Accept' => 'application/xml'])
                ->setMethod('get')
                ->setUrl(Yii::$app->params['apiurl'].'/load')
                ->setData([
                    'access-token' => Yii::$app->params['access_token'],
                    'file' => $filename,
                ])
                ->send();
            if ($response->isOk) {
                $obmen=new ObmenAfilter($response->data, $filename);

                try {
                    $obmen->importorders();
                } catch (\Exception $e) {
                    Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['supportEmail'])
                    ->setTo('web@laitovo.ru')
                    ->setSubject( Yii::t('app', 'Проблема синхронизации') )
                    ->setTextBody((string)$e)
                    ->send();
                }
            }
        }
    }

}
