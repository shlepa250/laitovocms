<?php

use yii\db\Schema;
use yii\db\Migration;

class m160330_073230_catalog extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%catalog}}', [
            'id' => $this->primaryKey(),
            'website_id' => $this->integer()->notNull()->comment('Сайт'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'count_on_page' => $this->integer()->defaultValue(24)->comment('Количество товаров на странице'),
            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('catalog_website_id','{{%catalog}}','website_id, status');
        $this->createIndex('catalog_sort','{{%catalog}}','sort');

        $this->addForeignKey('catalog_author_id','{{%catalog}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('catalog_updater_id','{{%catalog}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('catalog_website_id','{{%catalog}}','website_id','{{%website}}','id','CASCADE','CASCADE');

        $this->addColumn('{{%alias}}', 'catalog_id', $this->integer()->comment('Каталог'));
        $this->addForeignKey('alias_catalog_id','{{%alias}}','catalog_id','{{%catalog}}','id','CASCADE','CASCADE');

        $this->createTable('{{%catalog_lang}}', [
            'catalog_id' => $this->integer()->notNull()->comment('Каталог'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),

            'image' => $this->string()->comment('Изображение'),
            'anons' => $this->text()->comment('Анонс'),
            'content' => $this->text()->comment('Контент'),

            'PRIMARY KEY(catalog_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('catalog_lang_catalog_id','{{%catalog_lang}}','catalog_id','{{%catalog}}','id','CASCADE','CASCADE');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createCatalog = $auth->createPermission('createCatalog');
        $createCatalog->description = 'Разрешение добавлять каталог';
        $auth->add($createCatalog);
        $readCatalog = $auth->createPermission('readCatalog');
        $readCatalog->description = 'Разрешение просматривать каталог';
        $auth->add($readCatalog);
        $updateCatalog = $auth->createPermission('updateCatalog');
        $updateCatalog->description = 'Разрешение редактировать каталог';
        $auth->add($updateCatalog);
        $deleteCatalog = $auth->createPermission('deleteCatalog');
        $deleteCatalog->description = 'Разрешение удалять каталог';
        $auth->add($deleteCatalog);

        $rule = $auth->getRule('isAuthor');
        $updateOwnCatalog = $auth->createPermission('updateOwnCatalog');
        $updateOwnCatalog->description = 'Разрешение редактировать созданный каталог';
        $updateOwnCatalog->ruleName = $rule->name;
        $auth->add($updateOwnCatalog);
        $auth->addChild($updateOwnCatalog, $updateCatalog);

        $catalogReader = $auth->createRole('catalogReader');
        $auth->add($catalogReader);
        $auth->addChild($catalogReader, $readCatalog);

        $catalogAuthor = $auth->createRole('catalogAuthor');
        $auth->add($catalogAuthor);
        $auth->addChild($catalogAuthor, $catalogReader);
        $auth->addChild($catalogAuthor, $createCatalog);
        $auth->addChild($catalogAuthor, $updateOwnCatalog);

        $catalogManager = $auth->createRole('catalogManager');
        $auth->add($catalogManager);
        $auth->addChild($catalogManager, $catalogAuthor);
        $auth->addChild($catalogManager, $updateCatalog);

        $catalogAdmin = $auth->createRole('catalogAdmin');
        $auth->add($catalogAdmin);
        $auth->addChild($catalogAdmin, $catalogManager);
        $auth->addChild($catalogAdmin, $deleteCatalog);

        $auth->addChild($admin, $catalogAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createCatalog = $auth->getPermission('createCatalog');
        $readCatalog = $auth->getPermission('readCatalog');
        $updateCatalog = $auth->getPermission('updateCatalog');
        $deleteCatalog = $auth->getPermission('deleteCatalog');
        $updateOwnCatalog = $auth->getPermission('updateOwnCatalog');

        $catalogReader = $auth->getRole('catalogReader');
        $catalogAuthor = $auth->getRole('catalogAuthor');
        $catalogManager = $auth->getRole('catalogManager');
        $catalogAdmin = $auth->getRole('catalogAdmin');

        $auth->removeChild($admin, $catalogAdmin);
        $auth->removeChildren($catalogAdmin);
        $auth->removeChildren($catalogManager);
        $auth->removeChildren($catalogAuthor);
        $auth->removeChildren($catalogReader);
        $auth->removeChildren($updateOwnCatalog);

        $auth->remove($catalogAdmin);
        $auth->remove($catalogManager);
        $auth->remove($catalogAuthor);
        $auth->remove($catalogReader);
        $auth->remove($updateOwnCatalog);
        $auth->remove($deleteCatalog);
        $auth->remove($updateCatalog);
        $auth->remove($readCatalog);
        $auth->remove($createCatalog);

        $this->delete('{{%catalog}}');

        $this->dropForeignKey('catalog_lang_catalog_id','{{%catalog_lang}}');
        $this->dropTable('{{%catalog_lang}}');

        $this->dropForeignKey('alias_catalog_id','{{%alias}}');
        $this->dropColumn('{{%alias}}', 'catalog_id');

        $this->dropForeignKey('catalog_website_id','{{%catalog}}');
        $this->dropForeignKey('catalog_updater_id','{{%catalog}}');
        $this->dropForeignKey('catalog_author_id','{{%catalog}}');

        $this->dropTable('{{%catalog}}');
    }
}
