<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_071616_team extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%team}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название команды'),
            'image' => $this->string()->comment('Изображение'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),
            'discount' => $this->money(19,2)->comment('Скидка'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('team_author_id','{{%team}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('team_updater_id','{{%team}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addColumn('{{%website}}', 'team_id', $this->integer()->comment('Команда'));
        $this->addForeignKey('website_team_id','{{%website}}','team_id','{{%team}}','id','CASCADE','CASCADE');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createTeam = $auth->createPermission('createTeam');
        $createTeam->description = 'Разрешение добавлять команду';
        $auth->add($createTeam);
        $readTeam = $auth->createPermission('readTeam');
        $readTeam->description = 'Разрешение просматривать команду';
        $auth->add($readTeam);
        $updateTeam = $auth->createPermission('updateTeam');
        $updateTeam->description = 'Разрешение редактировать команду';
        $auth->add($updateTeam);
        $deleteTeam = $auth->createPermission('deleteTeam');
        $deleteTeam->description = 'Разрешение удалять команду';
        $auth->add($deleteTeam);

        $rule = $auth->getRule('isAuthor');
        $updateOwnTeam = $auth->createPermission('updateOwnTeam');
        $updateOwnTeam->description = 'Разрешение редактировать созданную команду';
        $updateOwnTeam->ruleName = $rule->name;
        $auth->add($updateOwnTeam);
        $auth->addChild($updateOwnTeam, $updateTeam);

        $teamReader = $auth->createRole('teamReader');
        $auth->add($teamReader);
        $auth->addChild($teamReader, $readTeam);

        $teamAuthor = $auth->createRole('teamAuthor');
        $auth->add($teamAuthor);
        $auth->addChild($teamAuthor, $teamReader);
        $auth->addChild($teamAuthor, $createTeam);
        $auth->addChild($teamAuthor, $updateOwnTeam);

        $teamManager = $auth->createRole('teamManager');
        $auth->add($teamManager);
        $auth->addChild($teamManager, $teamAuthor);
        $auth->addChild($teamManager, $updateTeam);

        $teamAdmin = $auth->createRole('teamAdmin');
        $auth->add($teamAdmin);
        $auth->addChild($teamAdmin, $teamManager);
        $auth->addChild($teamAdmin, $deleteTeam);

        $auth->addChild($admin, $teamAdmin);

        $this->createTable('{{%user_team}}', [
            'user_id' => $this->integer()->notNull(),
            'team_id' => $this->integer()->notNull(),
            'PRIMARY KEY(user_id, team_id)'
        ], $tableOptions);

        $this->createIndex('idx_user_team_user_id', '{{%user_team}}', 'user_id');
        $this->createIndex('idx_user_team_team_id', '{{%user_team}}', 'team_id');

        $this->addForeignKey('fk_user_team_user_id', '{{%user_team}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_team_team_id', '{{%user_team}}', 'team_id', '{{%team}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%team_client}}', [
            'team_id' => $this->integer()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'PRIMARY KEY(team_id, client_id)'
        ], $tableOptions);

        $this->createIndex('idx_team_client_team_id', '{{%team_client}}', 'team_id');
        $this->createIndex('idx_team_client_client_id', '{{%team_client}}', 'client_id');

        $this->addForeignKey('fk_team_client_team_id', '{{%team_client}}', 'team_id', '{{%team}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_team_client_client_id', '{{%team_client}}', 'client_id', '{{%team}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->delete('{{%team_client}}');
        $this->dropForeignKey('fk_team_client_team_id','{{%team_client}}');
        $this->dropForeignKey('fk_team_client_client_id','{{%team_client}}');
        $this->dropTable('{{%team_client}}');

        $this->delete('{{%user_team}}');
        $this->dropForeignKey('fk_user_team_user_id','{{%user_team}}');
        $this->dropForeignKey('fk_user_team_team_id','{{%user_team}}');
        $this->dropTable('{{%user_team}}');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createTeam = $auth->getPermission('createTeam');
        $readTeam = $auth->getPermission('readTeam');
        $updateTeam = $auth->getPermission('updateTeam');
        $deleteTeam = $auth->getPermission('deleteTeam');
        $updateOwnTeam = $auth->getPermission('updateOwnTeam');

        $teamReader = $auth->getRole('teamReader');
        $teamAuthor = $auth->getRole('teamAuthor');
        $teamManager = $auth->getRole('teamManager');
        $teamAdmin = $auth->getRole('teamAdmin');

        $auth->removeChild($admin, $teamAdmin);
        $auth->removeChildren($teamAdmin);
        $auth->removeChildren($teamManager);
        $auth->removeChildren($teamAuthor);
        $auth->removeChildren($teamReader);
        $auth->removeChildren($updateOwnTeam);

        $auth->remove($teamAdmin);
        $auth->remove($teamManager);
        $auth->remove($teamAuthor);
        $auth->remove($teamReader);
        $auth->remove($updateOwnTeam);
        $auth->remove($deleteTeam);
        $auth->remove($updateTeam);
        $auth->remove($readTeam);
        $auth->remove($createTeam);

        $this->delete('{{%team}}');

        $this->dropForeignKey('website_team_id','{{%website}}');
        $this->dropColumn('{{%website}}', 'team_id');

        $this->dropForeignKey('team_updater_id','{{%team}}');
        $this->dropForeignKey('team_author_id','{{%team}}');

        $this->dropTable('{{%team}}');
    }
}
