<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_103543_website extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%website}}', [
            'id' => $this->primaryKey(),
            'subdomain' => $this->string()->comment('Поддомен'),

            'name' => $this->string()->comment('Название'),
            'title' => $this->string()->comment('Заголовок'),
            'description' => $this->string()->comment('Описание'),
            'keywords' => $this->string()->comment('Ключевые слова'),
            'metatag' => $this->text()->comment('Мета теги'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'expires_at' => $this->integer()->comment('Срок действия'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'template' => $this->string()->comment('Шаблон'),

            'logo' => $this->string()->comment('Логотип'),
            'css' => $this->text()->comment('CSS'),
            'js' => $this->text()->comment('JS'),
            'html' => $this->text()->comment('HTML'),
            'robots' => $this->text()->comment('ROBOTS'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('website_subdomain','{{%website}}','subdomain');
        $this->createIndex('website_status','{{%website}}','status');
        $this->createIndex('website_expires_at','{{%website}}','expires_at');

        $this->addForeignKey('website_author_id','{{%website}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('website_updater_id','{{%website}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('user_website_id','{{%user}}','website_id','{{%website}}','id','CASCADE','CASCADE');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createWebsite = $auth->createPermission('createWebsite');
        $createWebsite->description = 'Разрешение добавлять сайты';
        $auth->add($createWebsite);
        $readWebsite = $auth->createPermission('readWebsite');
        $readWebsite->description = 'Разрешение просматривать сайты';
        $auth->add($readWebsite);
        $updateWebsite = $auth->createPermission('updateWebsite');
        $updateWebsite->description = 'Разрешение редактировать сайты';
        $auth->add($updateWebsite);
        $deleteWebsite = $auth->createPermission('deleteWebsite');
        $deleteWebsite->description = 'Разрешение удалять сайты';
        $auth->add($deleteWebsite);

        $rule = $auth->getRule('isAuthor');
        $updateOwnWebsite = $auth->createPermission('updateOwnWebsite');
        $updateOwnWebsite->description = 'Разрешение редактировать созданные сайты';
        $updateOwnWebsite->ruleName = $rule->name;
        $auth->add($updateOwnWebsite);
        $auth->addChild($updateOwnWebsite, $updateWebsite);

        $websiteReader = $auth->createRole('websiteReader');
        $auth->add($websiteReader);
        $auth->addChild($websiteReader, $readWebsite);

        $websiteAuthor = $auth->createRole('websiteAuthor');
        $auth->add($websiteAuthor);
        $auth->addChild($websiteAuthor, $websiteReader);
        $auth->addChild($websiteAuthor, $createWebsite);
        $auth->addChild($websiteAuthor, $updateOwnWebsite);

        $websiteManager = $auth->createRole('websiteManager');
        $auth->add($websiteManager);
        $auth->addChild($websiteManager, $websiteAuthor);
        $auth->addChild($websiteManager, $updateWebsite);

        $websiteAdmin = $auth->createRole('websiteAdmin');
        $auth->add($websiteAdmin);
        $auth->addChild($websiteAdmin, $websiteManager);
        $auth->addChild($websiteAdmin, $deleteWebsite);

        $auth->addChild($admin, $websiteAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createWebsite = $auth->getPermission('createWebsite');
        $readWebsite = $auth->getPermission('readWebsite');
        $updateWebsite = $auth->getPermission('updateWebsite');
        $deleteWebsite = $auth->getPermission('deleteWebsite');
        $updateOwnWebsite = $auth->getPermission('updateOwnWebsite');

        $websiteReader = $auth->getRole('websiteReader');
        $websiteAuthor = $auth->getRole('websiteAuthor');
        $websiteManager = $auth->getRole('websiteManager');
        $websiteAdmin = $auth->getRole('websiteAdmin');

        $auth->removeChild($admin, $websiteAdmin);
        $auth->removeChildren($websiteAdmin);
        $auth->removeChildren($websiteManager);
        $auth->removeChildren($websiteAuthor);
        $auth->removeChildren($websiteReader);
        $auth->removeChildren($updateOwnWebsite);

        $auth->remove($websiteAdmin);
        $auth->remove($websiteManager);
        $auth->remove($websiteAuthor);
        $auth->remove($websiteReader);
        $auth->remove($updateOwnWebsite);
        $auth->remove($deleteWebsite);
        $auth->remove($updateWebsite);
        $auth->remove($readWebsite);
        $auth->remove($createWebsite);

        $this->delete('{{%website}}');

        $this->dropForeignKey('user_website_id','{{%user}}');
        $this->dropForeignKey('website_updater_id','{{%website}}');
        $this->dropForeignKey('website_author_id','{{%website}}');

        $this->dropTable('{{%website}}');

    }
}
