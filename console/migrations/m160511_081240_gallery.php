<?php

use yii\db\Schema;
use yii\db\Migration;

class m160511_081240_gallery extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%gallery}}', [
            'id' => $this->primaryKey(),
            'website_id' => $this->integer()->notNull()->comment('Сайт'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'count_on_page' => $this->integer()->defaultValue(24)->comment('Количество фото/видео на странице'),
            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('i_gallery_website_id','{{%gallery}}','website_id, status');
        $this->createIndex('i_gallery_sort','{{%gallery}}','sort');

        $this->addForeignKey('fk_gallery_author_id','{{%gallery}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_gallery_updater_id','{{%gallery}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_gallery_website_id','{{%gallery}}','website_id','{{%website}}','id','CASCADE','CASCADE');

        $this->addColumn('{{%alias}}', 'gallery_id', $this->integer()->comment('Галерея'));
        $this->addForeignKey('fk_alias_gallery_id','{{%alias}}','gallery_id','{{%gallery}}','id','CASCADE','CASCADE');

        $this->createTable('{{%gallery_lang}}', [
            'gallery_id' => $this->integer()->notNull()->comment('Галерея'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),

            'image' => $this->string()->comment('Изображение'),
            'anons' => $this->text()->comment('Анонс'),
            'content' => $this->text()->comment('Контент'),
            'PRIMARY KEY(gallery_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('fk_gallery_lang_gallery_id','{{%gallery_lang}}','gallery_id','{{%gallery}}','id','CASCADE','CASCADE');

        //ФОТО/ВИДЕО, просто таблицу назову photo
        $this->createTable('{{%photo}}', [
            'id' => $this->primaryKey(),
            'gallery_id' => $this->integer()->notNull()->comment('Галерея'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text()->comment('JSON'),
        ], $tableOptions);

        $this->createIndex('idx_photo_gallery_id','{{%photo}}','gallery_id, status, sort');

        $this->addForeignKey('fk_photo_author_id','{{%photo}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_photo_updater_id','{{%photo}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_photo_gallery_id','{{%photo}}','gallery_id','{{%gallery}}','id','CASCADE','CASCADE');

        $this->createTable('{{%photo_lang}}', [
            'photo_id' => $this->integer()->notNull()->comment('Фото/видео'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),
            'photo' => $this->string()->comment('Фото'),
            'video' => $this->string()->comment('Видео'),
            'html' => $this->text()->comment('HTML'),

            'PRIMARY KEY(photo_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('photo_lang_photo_id','{{%photo_lang}}','photo_id','{{%photo}}','id','CASCADE','CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createGallery = $auth->createPermission('createGallery');
        $createGallery->description = 'Разрешение добавлять галерею';
        $auth->add($createGallery);
        $readGallery = $auth->createPermission('readGallery');
        $readGallery->description = 'Разрешение просматривать галерею';
        $auth->add($readGallery);
        $updateGallery = $auth->createPermission('updateGallery');
        $updateGallery->description = 'Разрешение редактировать галерею';
        $auth->add($updateGallery);
        $deleteGallery = $auth->createPermission('deleteGallery');
        $deleteGallery->description = 'Разрешение удалять галерею';
        $auth->add($deleteGallery);

        $rule = $auth->getRule('isAuthor');
        $updateOwnGallery = $auth->createPermission('updateOwnGallery');
        $updateOwnGallery->description = 'Разрешение редактировать созданную галерею';
        $updateOwnGallery->ruleName = $rule->name;
        $auth->add($updateOwnGallery);
        $auth->addChild($updateOwnGallery, $updateGallery);

        $galleryReader = $auth->createRole('galleryReader');
        $auth->add($galleryReader);
        $auth->addChild($galleryReader, $readGallery);

        $galleryAuthor = $auth->createRole('galleryAuthor');
        $auth->add($galleryAuthor);
        $auth->addChild($galleryAuthor, $galleryReader);
        $auth->addChild($galleryAuthor, $createGallery);
        $auth->addChild($galleryAuthor, $updateOwnGallery);

        $galleryManager = $auth->createRole('galleryManager');
        $auth->add($galleryManager);
        $auth->addChild($galleryManager, $galleryAuthor);
        $auth->addChild($galleryManager, $updateGallery);

        $galleryAdmin = $auth->createRole('galleryAdmin');
        $auth->add($galleryAdmin);
        $auth->addChild($galleryAdmin, $galleryManager);
        $auth->addChild($galleryAdmin, $deleteGallery);

        $auth->addChild($admin, $galleryAdmin);

    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createGallery = $auth->getPermission('createGallery');
        $readGallery = $auth->getPermission('readGallery');
        $updateGallery = $auth->getPermission('updateGallery');
        $deleteGallery = $auth->getPermission('deleteGallery');
        $updateOwnGallery = $auth->getPermission('updateOwnGallery');

        $galleryReader = $auth->getRole('galleryReader');
        $galleryAuthor = $auth->getRole('galleryAuthor');
        $galleryManager = $auth->getRole('galleryManager');
        $galleryAdmin = $auth->getRole('galleryAdmin');

        $auth->removeChild($admin, $galleryAdmin);
        $auth->removeChildren($galleryAdmin);
        $auth->removeChildren($galleryManager);
        $auth->removeChildren($galleryAuthor);
        $auth->removeChildren($galleryReader);
        $auth->removeChildren($updateOwnGallery);

        $auth->remove($galleryAdmin);
        $auth->remove($galleryManager);
        $auth->remove($galleryAuthor);
        $auth->remove($galleryReader);
        $auth->remove($updateOwnGallery);
        $auth->remove($deleteGallery);
        $auth->remove($updateGallery);
        $auth->remove($readGallery);
        $auth->remove($createGallery);

        $this->delete('{{%photo}}');

        $this->dropForeignKey('photo_lang_photo_id','{{%photo_lang}}');
        $this->dropTable('{{%photo_lang}}');

        $this->dropForeignKey('fk_photo_gallery_id','{{%photo}}');
        $this->dropForeignKey('fk_photo_updater_id','{{%photo}}');
        $this->dropForeignKey('fk_photo_author_id','{{%photo}}');

        $this->dropTable('{{%photo}}');


        $this->delete('{{%gallery}}');

        $this->dropForeignKey('fk_gallery_lang_gallery_id','{{%gallery_lang}}');
        $this->dropTable('{{%gallery_lang}}');

        $this->dropForeignKey('fk_alias_gallery_id','{{%alias}}');
        $this->dropColumn('{{%alias}}', 'gallery_id');

        $this->dropForeignKey('fk_gallery_website_id','{{%gallery}}');
        $this->dropForeignKey('fk_gallery_updater_id','{{%gallery}}');
        $this->dropForeignKey('fk_gallery_author_id','{{%gallery}}');

        $this->dropTable('{{%gallery}}');

    }
}
