<?php

use yii\db\Schema;
use yii\db\Migration;

class m160513_084735_promo extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%promo}}', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer()->notNull()->comment('Команда'),

            'name' => $this->string()->comment('Название'),
            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),
            'main' => $this->boolean()->defaultValue(false)->comment('На главной'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
            'expires_at' => $this->integer()->comment('Срок действия'),

            'discount' => $this->decimal(19,4)->comment('Скидка'),
            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('i_promo_team_id','{{%promo}}','team_id, status');
        $this->createIndex('i_promo_sort','{{%promo}}','sort');

        $this->addForeignKey('fk_promo_author_id','{{%promo}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('fk_promo_updater_id','{{%promo}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('fk_promo_team_id','{{%promo}}','team_id','{{%team}}','id','CASCADE','CASCADE');


        $this->createTable('{{%product_promo}}', [
            'product_id' => $this->integer()->notNull(),
            'promo_id' => $this->integer()->notNull(),
            'PRIMARY KEY(product_id, promo_id)'
        ], $tableOptions);

        $this->createIndex('idx_product_promo_product_id', '{{%product_promo}}', 'product_id');
        $this->createIndex('idx_product_promo_promo_id', '{{%product_promo}}', 'promo_id');

        $this->addForeignKey('fk_product_promo_product_id', '{{%product_promo}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_product_promo_promo_id', '{{%product_promo}}', 'promo_id', '{{%promo}}', 'id', 'CASCADE', 'CASCADE');


        $this->createTable('{{%region_promo}}', [
            'region_id' => $this->integer()->notNull(),
            'promo_id' => $this->integer()->notNull(),
            'PRIMARY KEY(region_id, promo_id)'
        ], $tableOptions);

        $this->createIndex('idx_region_promo_region_id', '{{%region_promo}}', 'region_id');
        $this->createIndex('idx_region_promo_promo_id', '{{%region_promo}}', 'promo_id');

        $this->addForeignKey('fk_region_promo_region_id', '{{%region_promo}}', 'region_id', '{{%region}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_region_promo_promo_id', '{{%region_promo}}', 'promo_id', '{{%promo}}', 'id', 'CASCADE', 'CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createPromo = $auth->createPermission('createPromo');
        $createPromo->description = 'Разрешение добавлять акции';
        $auth->add($createPromo);
        $readPromo = $auth->createPermission('readPromo');
        $readPromo->description = 'Разрешение просматривать акции';
        $auth->add($readPromo);
        $updatePromo = $auth->createPermission('updatePromo');
        $updatePromo->description = 'Разрешение редактировать акции';
        $auth->add($updatePromo);
        $deletePromo = $auth->createPermission('deletePromo');
        $deletePromo->description = 'Разрешение удалять акции';
        $auth->add($deletePromo);

        $rule = $auth->getRule('isAuthor');
        $updateOwnPromo = $auth->createPermission('updateOwnPromo');
        $updateOwnPromo->description = 'Разрешение редактировать созданные акции';
        $updateOwnPromo->ruleName = $rule->name;
        $auth->add($updateOwnPromo);
        $auth->addChild($updateOwnPromo, $updatePromo);

        $promoReader = $auth->createRole('promoReader');
        $auth->add($promoReader);
        $auth->addChild($promoReader, $readPromo);

        $promoAuthor = $auth->createRole('promoAuthor');
        $auth->add($promoAuthor);
        $auth->addChild($promoAuthor, $promoReader);
        $auth->addChild($promoAuthor, $createPromo);
        $auth->addChild($promoAuthor, $updateOwnPromo);

        $promoManager = $auth->createRole('promoManager');
        $auth->add($promoManager);
        $auth->addChild($promoManager, $promoAuthor);
        $auth->addChild($promoManager, $updatePromo);

        $promoAdmin = $auth->createRole('promoAdmin');
        $auth->add($promoAdmin);
        $auth->addChild($promoAdmin, $promoManager);
        $auth->addChild($promoAdmin, $deletePromo);

        $auth->addChild($admin, $promoAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createPromo = $auth->getPermission('createPromo');
        $readPromo = $auth->getPermission('readPromo');
        $updatePromo = $auth->getPermission('updatePromo');
        $deletePromo = $auth->getPermission('deletePromo');
        $updateOwnPromo = $auth->getPermission('updateOwnPromo');

        $promoReader = $auth->getRole('promoReader');
        $promoAuthor = $auth->getRole('promoAuthor');
        $promoManager = $auth->getRole('promoManager');
        $promoAdmin = $auth->getRole('promoAdmin');

        $auth->removeChild($admin, $promoAdmin);
        $auth->removeChildren($promoAdmin);
        $auth->removeChildren($promoManager);
        $auth->removeChildren($promoAuthor);
        $auth->removeChildren($promoReader);
        $auth->removeChildren($updateOwnPromo);

        $auth->remove($promoAdmin);
        $auth->remove($promoManager);
        $auth->remove($promoAuthor);
        $auth->remove($promoReader);
        $auth->remove($updateOwnPromo);
        $auth->remove($deletePromo);
        $auth->remove($updatePromo);
        $auth->remove($readPromo);
        $auth->remove($createPromo);

        $this->delete('{{%product_promo}}');
        $this->dropForeignKey('fk_product_promo_product_id','{{%product_promo}}');
        $this->dropForeignKey('fk_product_promo_promo_id','{{%product_promo}}');
        $this->dropTable('{{%product_promo}}');


        $this->delete('{{%region_promo}}');
        $this->dropForeignKey('fk_region_promo_region_id','{{%region_promo}}');
        $this->dropForeignKey('fk_region_promo_promo_id','{{%region_promo}}');
        $this->dropTable('{{%region_promo}}');


        $this->delete('{{%promo}}');

        $this->dropForeignKey('fk_promo_team_id','{{%promo}}');
        $this->dropForeignKey('fk_promo_updater_id','{{%promo}}');
        $this->dropForeignKey('fk_promo_author_id','{{%promo}}');

        $this->dropTable('{{%promo}}');
    }
}
