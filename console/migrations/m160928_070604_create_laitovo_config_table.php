<?php

use yii\db\Migration;

/**
 * Handles the creation for table `laitovo_config`.
 */
class m160928_070604_create_laitovo_config_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('laitovo_config', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'json' => $this->text(),
        ]);
        $auth = Yii::$app->AuthManager;
        $admin = $auth->getRole('admin');

        $updateConfig = $auth->createPermission('updateConfig');
        $auth->add($updateConfig);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('laitovo_config');
    }
}
