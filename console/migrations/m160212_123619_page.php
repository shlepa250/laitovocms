<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_123619_page extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(),
            'website_id' => $this->integer()->notNull()->comment('Сайт'),
            'type' => $this->string()->comment('Тип страницы'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),
            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('page_website_id','{{%page}}','website_id, status');
        $this->createIndex('page_sort','{{%page}}','sort');

        $this->addForeignKey('page_author_id','{{%page}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('page_updater_id','{{%page}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('page_website_id','{{%page}}','website_id','{{%website}}','id','CASCADE','CASCADE');

        $this->addColumn('{{%alias}}', 'page_id', $this->integer()->comment('Страница'));
        $this->addForeignKey('alias_page_id','{{%alias}}','page_id','{{%page}}','id','CASCADE','CASCADE');

        $this->createTable('{{%page_lang}}', [
            'page_id' => $this->integer()->notNull()->comment('Страница'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),

            'image' => $this->string()->comment('Изображение'),
            'anons' => $this->text()->comment('Анонс'),
            'content' => $this->text()->comment('Контент'),
            'PRIMARY KEY(page_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('page_lang_page_id','{{%page_lang}}','page_id','{{%page}}','id','CASCADE','CASCADE');


        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createPage = $auth->createPermission('createPage');
        $createPage->description = 'Разрешение добавлять страницы';
        $auth->add($createPage);
        $readPage = $auth->createPermission('readPage');
        $readPage->description = 'Разрешение просматривать страницы';
        $auth->add($readPage);
        $updatePage = $auth->createPermission('updatePage');
        $updatePage->description = 'Разрешение редактировать страницы';
        $auth->add($updatePage);
        $deletePage = $auth->createPermission('deletePage');
        $deletePage->description = 'Разрешение удалять страницы';
        $auth->add($deletePage);

        $rule = $auth->getRule('isAuthor');
        $updateOwnPage = $auth->createPermission('updateOwnPage');
        $updateOwnPage->description = 'Разрешение редактировать созданные страницы';
        $updateOwnPage->ruleName = $rule->name;
        $auth->add($updateOwnPage);
        $auth->addChild($updateOwnPage, $updatePage);

        $pageReader = $auth->createRole('pageReader');
        $auth->add($pageReader);
        $auth->addChild($pageReader, $readPage);

        $pageAuthor = $auth->createRole('pageAuthor');
        $auth->add($pageAuthor);
        $auth->addChild($pageAuthor, $pageReader);
        $auth->addChild($pageAuthor, $createPage);
        $auth->addChild($pageAuthor, $updateOwnPage);

        $pageManager = $auth->createRole('pageManager');
        $auth->add($pageManager);
        $auth->addChild($pageManager, $pageAuthor);
        $auth->addChild($pageManager, $updatePage);

        $pageAdmin = $auth->createRole('pageAdmin');
        $auth->add($pageAdmin);
        $auth->addChild($pageAdmin, $pageManager);
        $auth->addChild($pageAdmin, $deletePage);

        $auth->addChild($admin, $pageAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createPage = $auth->getPermission('createPage');
        $readPage = $auth->getPermission('readPage');
        $updatePage = $auth->getPermission('updatePage');
        $deletePage = $auth->getPermission('deletePage');
        $updateOwnPage = $auth->getPermission('updateOwnPage');

        $pageReader = $auth->getRole('pageReader');
        $pageAuthor = $auth->getRole('pageAuthor');
        $pageManager = $auth->getRole('pageManager');
        $pageAdmin = $auth->getRole('pageAdmin');

        $auth->removeChild($admin, $pageAdmin);
        $auth->removeChildren($pageAdmin);
        $auth->removeChildren($pageManager);
        $auth->removeChildren($pageAuthor);
        $auth->removeChildren($pageReader);
        $auth->removeChildren($updateOwnPage);

        $auth->remove($pageAdmin);
        $auth->remove($pageManager);
        $auth->remove($pageAuthor);
        $auth->remove($pageReader);
        $auth->remove($updateOwnPage);
        $auth->remove($deletePage);
        $auth->remove($updatePage);
        $auth->remove($readPage);
        $auth->remove($createPage);

        $this->delete('{{%page}}');

        $this->dropForeignKey('page_lang_page_id','{{%page_lang}}');
        $this->dropTable('{{%page_lang}}');

        $this->dropForeignKey('alias_page_id','{{%alias}}');
        $this->dropColumn('{{%alias}}', 'page_id');

        $this->dropForeignKey('page_website_id','{{%page}}');
        $this->dropForeignKey('page_updater_id','{{%page}}');
        $this->dropForeignKey('page_author_id','{{%page}}');

        $this->dropTable('{{%page}}');
    }
}
