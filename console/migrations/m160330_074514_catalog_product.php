<?php

use yii\db\Schema;
use yii\db\Migration;

class m160330_074514_catalog_product extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%catalog_product}}', [
            'id' => $this->primaryKey(),
            'catalog_id' => $this->integer()->notNull()->comment('Каталог'),
            'product_id' => $this->integer()->comment('Товар'),

            'sort' => $this->integer()->defaultValue(100)->comment('Сортировка'),
            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),
            'rating' => $this->integer()->defaultValue(0)->comment('Рейтинг'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('catalog_product_catalog_id','{{%catalog_product}}','catalog_id, status');
        $this->createIndex('catalog_product_sort','{{%catalog_product}}','sort');
        $this->createIndex('catalog_product_rating','{{%catalog_product}}','rating');

        $this->addForeignKey('catalog_product_author_id','{{%catalog_product}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('catalog_product_updater_id','{{%catalog_product}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('catalog_product_catalog_id','{{%catalog_product}}','catalog_id','{{%catalog}}','id','CASCADE','CASCADE');
        $this->addForeignKey('catalog_product_product_id','{{%catalog_product}}','product_id','{{%product}}','id','SET NULL','CASCADE');

        $this->addColumn('{{%alias}}', 'product_id', $this->integer()->comment('Продукт'));
        $this->addForeignKey('alias_product_id','{{%alias}}','product_id','{{%catalog_product}}','id','CASCADE','CASCADE');

        $this->createTable('{{%catalog_product_lang}}', [
            'catalog_product_id' => $this->integer()->notNull()->comment('Продукт'),
            'lang' => $this->string()->comment('Язык'),

            'name' => $this->string()->comment('Название'),

            'image' => $this->string()->comment('Изображение'),
            'anons' => $this->text()->comment('Анонс'),
            'content' => $this->text()->comment('Контент'),

            'PRIMARY KEY(catalog_product_id, lang)'
        ], $tableOptions);

        $this->addForeignKey('catalog_product_lang_catalog_product_id','{{%catalog_product_lang}}','catalog_product_id','{{%catalog_product}}','id','CASCADE','CASCADE');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createCProduct = $auth->createPermission('createCProduct');
        $createCProduct->description = 'Разрешение добавлять товары';
        $auth->add($createCProduct);
        $readCProduct = $auth->createPermission('readCProduct');
        $readCProduct->description = 'Разрешение просматривать товары';
        $auth->add($readCProduct);
        $updateCProduct = $auth->createPermission('updateCProduct');
        $updateCProduct->description = 'Разрешение редактировать товары';
        $auth->add($updateCProduct);
        $deleteCProduct = $auth->createPermission('deleteCProduct');
        $deleteCProduct->description = 'Разрешение удалять товары';
        $auth->add($deleteCProduct);

        $rule = $auth->getRule('isAuthor');
        $updateOwnCProduct = $auth->createPermission('updateOwnCProduct');
        $updateOwnCProduct->description = 'Разрешение редактировать созданные товары';
        $updateOwnCProduct->ruleName = $rule->name;
        $auth->add($updateOwnCProduct);
        $auth->addChild($updateOwnCProduct, $updateCProduct);

        $cproductReader = $auth->createRole('cproductReader');
        $auth->add($cproductReader);
        $auth->addChild($cproductReader, $readCProduct);

        $cproductAuthor = $auth->createRole('cproductAuthor');
        $auth->add($cproductAuthor);
        $auth->addChild($cproductAuthor, $cproductReader);
        $auth->addChild($cproductAuthor, $createCProduct);
        $auth->addChild($cproductAuthor, $updateOwnCProduct);

        $cproductManager = $auth->createRole('cproductManager');
        $auth->add($cproductManager);
        $auth->addChild($cproductManager, $cproductAuthor);
        $auth->addChild($cproductManager, $updateCProduct);

        $cproductAdmin = $auth->createRole('cproductAdmin');
        $auth->add($cproductAdmin);
        $auth->addChild($cproductAdmin, $cproductManager);
        $auth->addChild($cproductAdmin, $deleteCProduct);

        $auth->addChild($admin, $cproductAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createCProduct = $auth->getPermission('createCProduct');
        $readCProduct = $auth->getPermission('readCProduct');
        $updateCProduct = $auth->getPermission('updateCProduct');
        $deleteCProduct = $auth->getPermission('deleteCProduct');
        $updateOwnCProduct = $auth->getPermission('updateOwnCProduct');

        $cproductReader = $auth->getRole('cproductReader');
        $cproductAuthor = $auth->getRole('cproductAuthor');
        $cproductManager = $auth->getRole('cproductManager');
        $cproductAdmin = $auth->getRole('cproductAdmin');

        $auth->removeChild($admin, $cproductAdmin);
        $auth->removeChildren($cproductAdmin);
        $auth->removeChildren($cproductManager);
        $auth->removeChildren($cproductAuthor);
        $auth->removeChildren($cproductReader);
        $auth->removeChildren($updateOwnCProduct);

        $auth->remove($cproductAdmin);
        $auth->remove($cproductManager);
        $auth->remove($cproductAuthor);
        $auth->remove($cproductReader);
        $auth->remove($updateOwnCProduct);
        $auth->remove($deleteCProduct);
        $auth->remove($updateCProduct);
        $auth->remove($readCProduct);
        $auth->remove($createCProduct);

        $this->delete('{{%catalog_product}}');

        $this->dropForeignKey('catalog_product_lang_catalog_product_id','{{%catalog_product_lang}}');
        $this->dropTable('{{%catalog_product_lang}}');

        $this->dropForeignKey('alias_product_id','{{%alias}}');
        $this->dropColumn('{{%alias}}', 'product_id');

        $this->dropForeignKey('catalog_product_product_id','{{%catalog_product}}');
        $this->dropForeignKey('catalog_product_catalog_id','{{%catalog_product}}');
        $this->dropForeignKey('catalog_product_updater_id','{{%catalog_product}}');
        $this->dropForeignKey('catalog_product_author_id','{{%catalog_product}}');

        $this->dropTable('{{%catalog_product}}');
    }
}
