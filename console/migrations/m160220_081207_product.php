<?php

use yii\db\Schema;
use yii\db\Migration;

class m160220_081207_product extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer()->notNull()->comment('Команда'),

            'name' => $this->string()->comment('Название товара'),
            'article' => $this->string()->comment('Артикул'),
            'unit' => $this->string()->comment('Ед. измерения'),

            'status' => $this->boolean()->defaultValue(true)->comment('Активен'),
            'quantity' => $this->decimal(19,4)->comment('Кол-во (остаток)'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата изменения'),
            'author_id' => $this->integer()->comment('Автор'),
            'updater_id' => $this->integer()->comment('Редактор'),

            'json' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('product_author_id','{{%product}}','author_id','{{%user}}','id','SET NULL','CASCADE');
        $this->addForeignKey('product_updater_id','{{%product}}','updater_id','{{%user}}','id','SET NULL','CASCADE');

        $this->addForeignKey('product_team_id','{{%product}}','team_id','{{%team}}','id','CASCADE','CASCADE');
        $this->createIndex('idx_product_article','{{%product}}','article');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createProduct = $auth->createPermission('createProduct');
        $createProduct->description = 'Разрешение добавлять товары';
        $auth->add($createProduct);
        $readProduct = $auth->createPermission('readProduct');
        $readProduct->description = 'Разрешение просматривать товары';
        $auth->add($readProduct);
        $updateProduct = $auth->createPermission('updateProduct');
        $updateProduct->description = 'Разрешение редактировать товары';
        $auth->add($updateProduct);
        $deleteProduct = $auth->createPermission('deleteProduct');
        $deleteProduct->description = 'Разрешение удалять товары';
        $auth->add($deleteProduct);

        $rule = $auth->getRule('isAuthor');
        $updateOwnProduct = $auth->createPermission('updateOwnProduct');
        $updateOwnProduct->description = 'Разрешение редактировать созданные товары';
        $updateOwnProduct->ruleName = $rule->name;
        $auth->add($updateOwnProduct);
        $auth->addChild($updateOwnProduct, $updateProduct);

        $productReader = $auth->createRole('productReader');
        $auth->add($productReader);
        $auth->addChild($productReader, $readProduct);

        $productAuthor = $auth->createRole('productAuthor');
        $auth->add($productAuthor);
        $auth->addChild($productAuthor, $productReader);
        $auth->addChild($productAuthor, $createProduct);
        $auth->addChild($productAuthor, $updateOwnProduct);

        $productManager = $auth->createRole('productManager');
        $auth->add($productManager);
        $auth->addChild($productManager, $productAuthor);
        $auth->addChild($productManager, $updateProduct);

        $productAdmin = $auth->createRole('productAdmin');
        $auth->add($productAdmin);
        $auth->addChild($productAdmin, $productManager);
        $auth->addChild($productAdmin, $deleteProduct);

        $auth->addChild($admin, $productAdmin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $createProduct = $auth->getPermission('createProduct');
        $readProduct = $auth->getPermission('readProduct');
        $updateProduct = $auth->getPermission('updateProduct');
        $deleteProduct = $auth->getPermission('deleteProduct');
        $updateOwnProduct = $auth->getPermission('updateOwnProduct');

        $productReader = $auth->getRole('productReader');
        $productAuthor = $auth->getRole('productAuthor');
        $productManager = $auth->getRole('productManager');
        $productAdmin = $auth->getRole('productAdmin');

        $auth->removeChild($admin, $productAdmin);
        $auth->removeChildren($productAdmin);
        $auth->removeChildren($productManager);
        $auth->removeChildren($productAuthor);
        $auth->removeChildren($productReader);
        $auth->removeChildren($updateOwnProduct);

        $auth->remove($productAdmin);
        $auth->remove($productManager);
        $auth->remove($productAuthor);
        $auth->remove($productReader);
        $auth->remove($updateOwnProduct);
        $auth->remove($deleteProduct);
        $auth->remove($updateProduct);
        $auth->remove($readProduct);
        $auth->remove($createProduct);

        $this->delete('{{%product}}');

        $this->dropForeignKey('product_team_id','{{%product}}');
        $this->dropForeignKey('product_updater_id','{{%product}}');
        $this->dropForeignKey('product_author_id','{{%product}}');

        $this->dropTable('{{%product}}');
    }
}
