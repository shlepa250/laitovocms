<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'urlManager' => [
            'class' => 'frontend\components\UrlManager',
            'rules' => [
                '/search' => 'site/search',
                '/login' => 'user/login',
                '/logout' => 'user/logout',
                '/registration' => 'user/registration',
                ['class' => 'frontend\components\url_rules\Pages'],
                ['class' => 'frontend\components\url_rules\Robots'],
                ['class' => 'frontend\components\url_rules\Feed'],
                ['class' => 'frontend\components\url_rules\Gallery'],
                ['class' => 'frontend\components\url_rules\Catalog'],
                ['class' => 'frontend\components\url_rules\Product'],
                ['class' => 'frontend\components\url_rules\News'],
                ['class' => 'frontend\components\url_rules\AjaxBasket'],
                ['class' => 'frontend\components\url_rules\Ajax'],
            ],
        ],
        'i18n' => [
            'translations' => [
                'site*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'ru',
                    'fileMap' => [
                        'site' => 'site.php',
                        'site/callback' => 'callback.php',
                        'site/geo' => 'geo.php',
                        'site/error' => 'error.php',
                        'site/user' => 'user.php',
                        'site/basket' => 'basket.php',
                    ],
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'baseUrl' => '@app/themes/laitovo',
                'pathMap' => [
                    '@app/views' => '@app/themes/laitovo/views',
                    '@app/widgets' => '@app/themes/laitovo/widgets'
                ],
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
