<?php
namespace frontend\components;
use common\models\website\Domain;
use Yii;

class UrlManager extends \yii\web\UrlManager {
    
    public $domain = null;
    public $webSite = null;
    public $alias = null;
    public $page = null;
    public $feed = null;
    public $news = null;
    public $catalog = null;
    public $product = null;
    public $gallery = null;

    private function _setTheme($themeName) {
        Yii::$app->view->theme->setBaseUrl('@app/themes/' . $themeName);
        Yii::$app->view->theme->pathMap = [
            '@app/views' => "@app/themes/{$themeName}/views",
            '@app/layouts' => "@app/themes/views/layouts/{$themeName}/widgets",
            '@app/widgets' => "@app/themes/{$themeName}/widgets",
        ];
    }

    private function _setAliasInfo(&$alias) {
        if ($alias->page_id) {
            $this->page = $alias->getPage()->andWhere(['page.status' => 1])->one();
        } elseif ($alias->feed_id) {
            $this->feed = $alias->getFeed()->andWhere(['feed.status' => 1])->one();
        } elseif ($alias->news_id) {
            $this->news = $alias->getNews()->andWhere(['news.status' => 1])->one();
        } elseif ($alias->catalog_id) {
            $this->catalog = $alias->getCatalog()->andWhere(['catalog.status' => 1])->one();
        } elseif ($alias->product_id) {
            $this->product = $alias->getProduct()->andWhere(['catalog_product.status' => 1])->one();
        } elseif ($alias->gallery_id) {
            $this->gallery = $alias->getGallery()->andWhere(['gallery.status' => 1])->one();
        }
    }

    public function init() {
        parent::init();
        $pathInfo = '/' . preg_replace('/\/+$/', '', Yii::$app->request->getPathInfo());

        $this->domain = $domain = Domain::findOne([
            'domain' => 'laitovo.ru',//Yii::$app->request->serverName,
            'status' => 1
        ]);
        if (is_null($domain)) {
            return;
        }
        if (!is_null($domain) && $domain->main == 0) {
            $mainDomain = Domain::findOne(['website_id' => $domain->website_id, 'main' => 1, 'status' => 1]);
            if (!is_null($mainDomain)) {
                return Yii::$app->getResponse()->redirect((Yii::$app->request->port == 80?'http://':'https://') . $mainDomain->domain . $pathInfo);
            }
        }
        $this->webSite = $webSite = $domain->getWebsite()->andWhere(['website.status' => 1])->one();
        if (is_null($webSite)) {
            return;
        }

        $this->_setTheme($webSite->template?:'laitovo');
        
        $this->alias = $alias = \common\models\website\Alias::findOne(['website_id' => $webSite->id, 'link' => $pathInfo, 'status' => 1]);
        if (is_null($alias)) {
            return;
        }
        $this->_setAliasInfo($alias);
    }
    
}