<?php
namespace frontend\components;

use common\models\order\Item;
use \common\models\order\Order;
use common\models\order\Product;

class Basket {

    /**
     * @var Order заказ
     */
    private $_order = null;

    /**
     * @var array регион
     */
    private $_region = null;

    /**
     * @var string валюта корзины
     */
    private $_valute = null;

    /**
     * @var \common\models\team\Team команда пользователя
     */
    private $_team = null;

    /**
     * @var Item[] товары в заказе [id => common\models\order\Item, ...]
     */
    private $_items = [];

    /**
     * Создание объекта корзины
     * @param integer $orderId идентификатор заказа для создания корзины
     * @param boolean $new получить последнюю или создать новую
     * @param boolean $canCreateNew разрешать ли создавать новый заказ, если последний не найден
     */
    public function __construct($orderId = null, $new = false, $canCreateNew = false) {
        $this->_region = GeoTool::getUserCity();
        $this->_team = \common\models\team\Team::findOne(1); /// !!!!
        if (is_null($orderId)) {
            if ($new) {
                $this->_getNew();
            } else {
                $this->_getLast($canCreateNew);
            }
        } else {
            $this->_getById($orderId);
        }
    }

    /**
     * Новая корзина пользователя
     * @return \frontend\components\Basket
     */
    public static function getNew() {
        return new self(null, true);
    }

    /**
     * Последняя корзина пользователя
     * @param boolean $canCreateNew создавать ли новый заказ, если последнего не существует
     * @return \frontend\components\Basket
     */
    public static function getLast($canCreateNew = false) {
        $basket = new self(null, false, $canCreateNew);
        return is_null($basket->_order)?null:$basket;
    }

    /**
     * Получить заказ по его идентификатору
     * @param integer $orderId идентификатор заказа
     * @return \frontend\components\Basket
     */
    public static function getById($orderId) {
        return new self($orderId, false);
    }

    /**
     * Проверят возможность изменения текущего заказа пользователем
     * @return boolean результат проверки
     */
    private function _checkAccess() {

        // TODO
        return true;
    }

    /**
     * Проверка валюты в заказе
     * @param string $valute название валюты
     * @throws \yii\web\ServerErrorHttpException попытка смешения разных валют в заказе
     */
    private function _checkValute($valute) {
        if (empty($this->_valute)) {
            $this->_valute = $valute;
        } elseif($valute !== $this->_valute) {
            throw new \yii\web\ServerErrorHttpException(500, 'Mixing currencies in order');
        }
    }

    private function _getNew() {
        $order = new Order();

        $order->user_id = \Yii::$app->user->id;
        $order->website_id = \Yii::$app->getUrlManager()->webSite->id;
        $order->region_id = $this->_region['id'];
        $order->team_id = $this->_team->id;
        $order->discount = 0;
//        $order->status = 0; ???
//        $order->condition = 0; ???

        $this->_valute = $order->currency;
        $this->_order = $order;
        $this->_order->save();
    }

    private function _getLast($canCreateNew) {
        $this->_order = Order::find()
                ->where([
                    'user_id' => \Yii::$app->user->id,
                    'website_id' => \Yii::$app->getUrlManager()->webSite->id,
                ])
                ->orderBy(['id' => SORT_DESC])
                ->one();
        $this->_valute = $this->_order->currency;
        if (is_null($this->_order) && $canCreateNew === false) {
            return false;
        } elseif(is_null($this->_order) === true && $canCreateNew === true) {
            $this->_getNew();
        }
        if ($this->_checkAccess() === false) {
            throw new \yii\web\HttpException(403, 'You can\'t edit this order');
        }
        $this->_loadItems();
    }

    private function _loadItems() {
        $this->_items = Item::find()
                ->indexBy('product_id')
                ->where(['order_id' => $this->_order->id])
                ->all();
    }

    private function _getById($orderId) {
        $this->_order = Order::findOne($orderId);
        if ($this->_checkAccess() === false) {
            throw new \yii\web\HttpException(403, 'You can\'t edit this order');
        }
        $this->_valute = $this->_order->currency;
        $this->_loadItems();
    }

    /**
     * Получает все товары в заказе
     * @return Item[] товары в текущем заказе
     */
    public function getItems() {
        return $this->_items;
    }

    /**
     * Добавляет товар в заказ
     * @param integer $productId идентификатор товара
     * @param integer $quantity количество добавляемого товара
     * @return boolean успешность добавления товара
     */
    public function addItem($productId, $quantity = 1) {
        if (isset($this->_items[$productId])) {
            return $this->editItem($productId, $this->_items[$productId]->quantity + $quantity);
        }

        $product = \common\models\order\Product::findOne(['id' => $productId, 'status' => 1]);
        if (is_null($product)) {
            return false;
        }
        $price = \common\models\order\Price::find()->where(['product_id' => $productId, 'region_id' => $this->_region['regions']])->one();
        $this->_checkValute($price->currency);
        $catalogProduct = \common\models\website\CatalogProduct::findOne($productId);
        $catalogProductLang = \common\models\website\CatalogProductLang::findOne([
            'catalog_product_id' => $catalogProduct->id,
            'lang' => \Yii::$app->language,
        ]);
        $item = new Item();
        $item->order_id = $this->_order->id;
        $item->product_id = $product->id;
        $item->name = $catalogProductLang->name;
        $item->price = $price->price;
        $item->quantity = $quantity;
        $this->_items[$productId] = $item;
        return true;
    }

    /**
     * Редактирование товара в заказе
     * @param integer $productId идентификатор товара
     * @param integer $newQuantity новое количество товара
     * @return boolean успешность редактирования товара
     */
    public function editItem($productId, $newQuantity) {
        if (isset($this->_items[$productId]) === false) {
            return $this->addItem($productId, $newQuantity);
        }

        $item = $this->_items[$productId];
        $product = $item->getProduct()->one();
        if ($product->status != 1) {
            $this->deleteItem($productId);
            return false;
        }
        $price = \common\models\order\Price::find()
                ->where(['product_id' => $productId, 'region_id' => $this->_region['regions']])
                ->one();
        $item->price = $price->price;
        $item->quantity = $newQuantity;
        $this->_items[$productId] = $item;
        return true;
    }

    /**
     * Удаляет товар из заказа
     * @param integer $productId идентификатор товара
     * @return boolean успешность удаления
     */
    public function deleteItem($productId) {
        if (isset($this->_items[$productId])) {
            unset($this->_items[$productId]);
        }
        return true;
    }

    /**
     * Сохранение измений в корзине
     * @return boolean результат сохранения корзины
     */
    public function save() {
        Item::deleteAll([
            'AND',
            ['order_id' => $this->_order->id],
            ['NOT IN', 'product_id', array_keys($this->_items)]
        ]);
        foreach ($this->_items as $item) {
            $item->save();
        }
        $this->_order->currency = $this->_valute;
        return $this->_order->save();
    }

    /**
     * Получает количество товаров в корзине
     * @return integer количество товаров в корзине
     */
    public function getTotalCount() {
        $count = 0;
        foreach ($this->_items as $item) {
            $count += $item->quantity;
        }
        return $count;
    }

    /**
     * Получает стоимость товаров в корзине
     * @return integer стоимость товаров в корзине
     */
    public function getTotalPrice() {
        $price = 0;
        foreach ($this->_items as $item) {
            $price += ($item->quantity * $item->price);
        }
        return $price;
    }

    /**
     * Получает валюту заказа
     * @return string валюта заказа
     */
    public function getValute() {
        return $this->_valute;
    }
}
