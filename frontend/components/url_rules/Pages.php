<?php
namespace frontend\components\url_rules;

class Pages implements \yii\web\UrlRuleInterface {
    
    private $_route = 'site/pages';

    public function createUrl($manager, $route, $params) {
        if ($this->_route !== $route || !isset($params['type'])) {
            return false;
        }
        $page = \common\models\website\Page::find()->where([
            'type' => $params['type'],
            'website_id' => $manager->webSite->id,
            'status' => 1,
        ])->one();
        if (is_null($page)) {
            return false;
        }
        $alias = \common\models\website\Alias::find()->where([
                'page_id' => $page->id,
                'status' => 1,
            ])->andWhere(['OR', ['=', 'lang', \Yii::$app->language], ['IS', 'lang', null]])
                ->one();
        if (is_null($alias)) {
            return false;
        }
        $url = preg_replace('/(^\/)/', '', $alias->link);
        unset($params['type']);
        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    public function parseRequest($manager, $request) {

        if (is_null($manager->page)) {
            return false;
        }

        //Тут проверить тип страницы

        return [$this->_route, []];
    }
}