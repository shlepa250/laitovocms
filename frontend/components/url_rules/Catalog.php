<?php
namespace frontend\components\url_rules;

class Catalog implements \yii\web\UrlRuleInterface {
    
    private $_route = 'site/catalog';

    private static $generatedUrls = [];

    public function createUrl($manager, $route, $params) {
        if ($this->_route !== $route || !isset($params['id'])) {
            return false;
        }

        $link = '';
        if (isset(self::$generatedUrls[$params['id']]) === false) {
            $alias = \common\models\website\Alias::find()->where(['catalog_id' => $params['id']])->one();
            if (is_null($alias)) {
                return false;
            }
            self::$generatedUrls[$params['id']] = $link = $alias->link;
        } elseif(self::$generatedUrls[$params['id']] == false) {
            return false;
        } else {
            $link = self::$generatedUrls[$params['id']];
        }
        unset($params['id']);
        $url = preg_replace('/(^\/)/', '', $link);
        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    public function parseRequest($manager, $request) {

        if (is_null($manager->catalog)) {
            return false;
        }

        //Тут проверить тип страницы

        return [$this->_route, []];
    }
}