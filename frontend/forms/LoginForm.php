<?php
namespace frontend\forms;

use Yii;
use yii\base\Model;
use common\models\User;

class LoginForm extends Model {

    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    public function rules() {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['password', 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels() {
        return [
            'email' => Yii::t('site/user', 'Email'),
            'password' => Yii::t('site/user', 'Пароль'),
            'rememberMe' => Yii::t('site/user', 'Запомнить'),
        ];
    }


    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('site/user', 'Неверный логин и/или пароль'));
            }
        }
    }

    public function login() {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    public function getUser() {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
}
