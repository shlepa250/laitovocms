<?php

use frontend\assets\LaitovoAsset;
?>

<section class="center clearfix">
    <div class="clearfix">
        <?= frontend\widgets\MainSlider::widget() ?>
        <div class="action">
            <div class="cart cart--mod">
                <div class="cart-body">
                    <div class="cart-pic"><a href="#"><img alt="" src="<?= LaitovoAsset::imgSrc('Layer-181.jpg') ?>"></a></div>
                    <div class="cart-name"><a href="#">Чехлы для хранения
                            колес колес</a></div>
                    <div class="cart-rating">
                        <i class="ico-0016-star-orange"></i>
                        <i class="ico-0016-star-orange"></i>
                        <i class="ico-0016-star-orange"></i>
                        <i class="ico-0016-star-orange"></i>
                        <i class="ico-0017-star"></i>
                    </div>
                    <div class="clearfix">
                        <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                        <div class="cart-price-old">1 699 <span class="rouble"><i class="hidden">₽</i></span></div>
                    </div>
                    <div class="cart-body-text hidden">
                        Запасной комплект
                        магнитных запасной
                        комплект магнитных
                    </div>
                    <div class="cart-label"><i class="ico-0015-plashka"><span class="cart-label-text">20%</span></i></div>
                </div>
            </div>
        </div>
    </div>

    <?= \frontend\widgets\ProductPopularMain::widget() ?>

    <?= \frontend\widgets\MainNews::widget() ?>

    <?= frontend\widgets\MainGallery::widget() ?>
</section>