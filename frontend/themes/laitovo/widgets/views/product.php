<div class="cart">
    <div class="cart-body">
        <div class="cart-pic">
            <?php if (!empty($image)) { ?>
                <a href="<?= $url ?>">
                    <img src="<?= $image ?>" alt="<?= $productLang->name ?>"/>
                </a>
            <?php } ?>
        </div>
        <div class="cart-name">
            <a href="<?= $url ?>"><?= $productLang->name ?></a>
        </div>
        <div class="cart-rating">
            <?php for($i = 0; $i < $product->rating; $i++) { ?>
                <i class="ico-0016-star-orange"></i>
            <?php } ?>
            <?php for(; $i < 5; $i++) { ?>
                <i class="ico-0017-star"></i>
            <?php } ?>
        </div>
        <div class="clearfix">
            <div class="cart-price">
                <?= number_format($price->price, 0, '.', ' ') ?> <span><?= $price->currency ?></span>
            </div>
            <span class="cart-btn f-right js-basket-add" data-id="<?= $product->id ?>"><i class="ico-0014-bas"></i></span>
        </div>
        <div class="cart-body-text hidden">
            <?= $productLang->anons ?>
        </div>
        <!--<div class="cart-label"><i class="ico-0027-layer-29"></i></div> !!!АКЦИЯ -->
    </div>
</div>