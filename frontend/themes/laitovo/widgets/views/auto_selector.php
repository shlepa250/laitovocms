<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if ($step === 1) { ?>
    <div class="nav-search">
        <div class="nav-search-head">Ваш авто: </div>
        <?= Html::dropDownList('filter[mark]', null, $categorieList, ['class' => 'js-styled-select w-100 js-step-1']) ?>
    </div>
    <div class="col no-margin">

        <?php
        $halfMarkList = array_chunk($categorieList, ceil(count($categorieList) / 2), true);
        foreach ($halfMarkList as $markLists) { ?>
            <div class="f-left w-50">
                <ul class="reset">
                    <?php foreach ($markLists as $markId => $markName) { ?>
                        <li class="nav-search-item">
                            <a class="nav-search-link js-step-1" data-mark-id="<?= $markId ?>" href="<?= Url::to(['site/catalog', 'id' => $catalog->id, 'filter[mark]' => $markId]) ?>"><?= $markName ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>

    </div>
<?php } elseif($step === 2) { ?>
    <div class="nav-search">
        <div class="nav-search-head">Модель <?= $mark->name ?>: </div>
        <?= Html::dropDownList('filter[mark]', null, $categorieList, ['class' => 'js-styled-select w-100 js-step-2']) ?>
    </div>
    <div class="col no-margin">
        
        <?php
        $halfModelList = array_chunk($categorieList, ceil(count($categorieList) / 2), true);
        foreach ($halfModelList as $modelLists) { ?>
            <div class="f-left w-50">
                <ul class="reset">
                    <?php foreach ($modelLists as $modelId => $modelName) {
                        $jsonInfo = yii\helpers\Json::decode($categorieJson[$modelId]);
                        ?>
                        <li class="nav-search-item">
                            <a class="nav-search-link js-step-2" title="<?= $modelName ?>" data-mark-id="<?= $mark->id ?>" data-model-id="<?= $modelId ?>" href="<?= Url::to(['site/catalog', 'id' => $catalog->id, 'filter[mark]' => $mark->id, 'filter[model]' => $modelId]) ?>">
                                <?= $mark->name ?> <?= $jsonInfo['model'] ?> <?= $jsonInfo['generation'] ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>

    </div>
<?php } elseif($step === 3) { ?>
    <div class="nav-search">
        <div class="nav-search-head">Ваш авто: </div>
        <div class="header-conf-body header-conf-body--satic">
            <div class="header-conf-table">
                <div class="header-conf-td">
                    <?php if ($autoInfo['photo']) {?>
                        <img alt="" src="<?= $autoInfo['photo'] ?>">
                    <?php } ?>
                </div>
                <div class="header-conf-td">
                    <div class="header-conf-head"><?= $model->name ?></div>
                    <i class="header-conf-ico ico-0023-refresh"></i>
                    <i class="header-conf-ico ico-0022-plus"></i>
                    <a href="#" class="header-conf-link">В гараж</a>
                </div>
            </div>
        </div>
        <span class="nav-search-back"><i class="ico-0004-arr-l"></i>
            <a href="<?= Url::to(['site/catalog', 'id' => $catalog->id]) ?>" class="link-dotted">Все авто</a>
        </span>
    </div>
    <div class="baner">
        <a href="#">
            <img src="<?= frontend\assets\LaitovoAsset::imgSrc('b.jpg') ?>" alt=""/>
        </a>
    </div>
<?php } ?>

