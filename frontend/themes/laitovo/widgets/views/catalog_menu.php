<?php
use frontend\assets\LaitovoAsset;
use yii\helpers\Url;
use frontend\widgets\AutoSelector;
?>
<ul class="nav-list">
    <li class="nav-item">
        <a class="nav-link" href="<?= Url::to(['site/catalog', 'id' => 5]) ?>">Каталог</a>
    </li>
    <?php foreach ($catalogs as $id => $catalog) { ?>
        <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['site/catalog', 'id' => $id]) ?>">
                <?= $catalogsLang[$id]->name ?>
            </a>
            
            <div class="nav-sub hidden clearfix" id="menu-catalog-<?= $id ?>">
                <div class="nav-sub-left js-menu-auto-selector" data-catalog-id="<?= $id ?>">

                    <?= AutoSelector::widget(['catalog' => $catalog]) ?>

                </div>
                <div class="nav-sub-line"></div>
                <div class="nav-sub-right">
                    <div class="nav-sub-right-head">Самые популярные</div>
                    <div class="nav-sub-catalog clearfix js-menu-popular-<?= $id ?>">
                        <?php foreach ($popularProducts[$id] as $popularProduct) { ?>
                            <div class="nav-sub-catalog-item">
                                <?= \frontend\widgets\Product::widget(['catalogProduct' => $popularProduct]) ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
<!--
    <li class="nav-item">
        <a class="nav-link" href="#">Автошторки Laitovo</a>
        <div class="nav-sub hidden clearfix">
            <div class="nav-sub-left">
                <div class="nav-search">
                    <div class="nav-search-head">Ваш авто: </div>
                    <select class="js-styled-select w-100">
                        <option value="10">Honda</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="10">Honda</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="10">Honda</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="10">Honda</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                        <option value="1">Костромская область</option>
                    </select>
                </div>
                <div class="col no-margin">
                    <div class="f-left w-50">
                        <ul class="reset">
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>Audi</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>BMW</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>Ford</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Honda</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>Audi</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>BMW</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>Ford</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Honda</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                        </ul>
                    </div>
                    <div class="f-left w-50">
                        <ul class="reset">
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>Audi</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>BMW</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>Ford</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Honda</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>Audi</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>BMW</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#"><b>Ford</b></a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Honda</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Hyundai</a></li>
                            <li class="nav-search-item"><a class="nav-search-link" href="#">Kia</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="nav-sub-line"></div>
            <div class="nav-sub-right">
                <div class="nav-sub-right-head">Самые популярные</div>
                <div class="nav-sub-catalog clearfix">
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                                <div class="cart-label"><i class="ico-0027-layer-29"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Автошторки Chiko</a>
        <div class="nav-sub hidden clearfix">
            <div class="nav-sub-left">
                <div class="nav-search">
                    <div class="nav-search-head">Ваш авто: </div>
                    <div class="header-conf-body header-conf-body--satic">
                        <div class="header-conf-table">
                            <div class="header-conf-td">
                                <a href="#"><img alt="" src="<?= LaitovoAsset::imgSrc('Layer-14.png') ?>"></a>
                            </div>
                            <div class="header-conf-td">
                                <div class="header-conf-head">Honda Accord VIII</div>
                                <i class="header-conf-ico ico-0023-refresh"></i>
                                <i class="header-conf-ico ico-0022-plus"></i>
                                <a href="#" class="header-conf-link">В гараж</a>
                            </div>
                        </div>
                    </div>
                    <span class="nav-search-back"><i class="ico-0004-arr-l"></i> <a href="#" class="link-dotted">Все авто</a></span>
                </div>
                <div class="baner"><a href="#"><img src="<?= LaitovoAsset::imgSrc('b.jpg') ?>" alt=""/></a></div>
            </div>
            <div class="nav-sub-line"></div>
            <div class="nav-sub-right">
                <div class="nav-sub-right-head">Самые популярные для Honda Accord VIII</div>
                <div class="nav-sub-catalog clearfix">
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                                <div class="cart-label"><i class="ico-0027-layer-29"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-sub-catalog-item">
                        <div class="cart">
                            <div class="cart-body">
                                <div class="cart-pic"><a href="#"><img src="<?= LaitovoAsset::imgSrc('Layer-19.jpg') ?>" alt=""/></a></div>
                                <div class="cart-name"><a href="#">Чехлы для хранения колес колес</a></div>
                                <div class="cart-rating">
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0016-star-orange"></i>
                                    <i class="ico-0017-star"></i>
                                </div>
                                <div class="clearfix">
                                    <div class="cart-price">1 499 <span class="rouble"><i class="hidden">₽</i></span></div>
                                    <a class="cart-btn f-right" href="#"><i class="ico-0014-bas"></i></a>
                                </div>
                                <div class="cart-body-text hidden">
                                    Запасной комплект
                                    магнитных запасной
                                    комплект магнитных
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nav-sub-right-brtn">
                    <a class="btn btn--midl btn--white" href="#">Смотреть все товары</a>
                </div>
            </div>
        </div>
    </li>
    <li class="nav-item"><a class="nav-link" href="#">Автошторки Don’t look</a></li>
    <li class="nav-item"><a class="nav-link" href="#">Москитные сетки</a></li>
    <li class="nav-item"><a class="nav-link" href="#">Аксессуары</a></li>-->
</ul>
