<?php
use frontend\widgets\Product;
?>

<div class="title"><?= Yii::t('site', 'Популярное') ?></div>
<div class="carusel-cart">
    <div class="carusel-cart-wrapper">
        <?php foreach ($products as $product) { ?>
            <div class="carusel-cart-slide">
                <?= Product::widget(['product' => $product]) ?>
            </div>
        <?php } ?>
    </div>
</div>