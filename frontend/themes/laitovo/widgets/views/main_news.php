<?php
use yii\helpers\Url;
?>
<div class="info-tabs">
    <div class="info-tabs-menu clearfix">
        <div class="info-tabs-menu-wrapper">
            <?php foreach ($feeds as $key => $feed) {
                    if (empty($newsList[$feed->id])) {
                    continue;
                } ?>
                <span class="info-tabs-item <?= $key==0?'active':'' ?>">
                    <?= $feedsInfo[$feed->id]->name ?>
                </span>
            <?php } ?>
        </div>
    </div>
    <div class="info-tabs-content clearfix">
        <?php foreach ($feeds as $key => $feed) {
            if (empty($newsList[$feed->id])) {
                continue;
            } ?>
            <div class="<?= $key==0?'active':'' ?>">
                <div class="news-anons">
                    <?php foreach ($newsList[$feed->id] as $new) { ?>
                        <div class="news-anons-item">
                            <div class="news-anons-date"><?= Yii::$app->formatter->asDate($new->date, 'long') ?></div>
                            <p>
                                <a href="<?= Url::to(['site/news', 'id' => $new->id]) ?>">
                                    <?= $newsInfo[$new->id]->anons ?>
                                </a>
                            </p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>