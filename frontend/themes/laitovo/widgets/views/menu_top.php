<nav class="header-nav">
    <ul class="header-nav-list reset">
        <?php foreach ($menus as $menu) { ?>
            <li class="header-nav-item">
                <a class="header-nav-link" href="<?= $menu->link ?>"><?= $menu->name ?></a>
            </li>
        <?php } ?>
    </ul>
</nav>