<?php
namespace frontend\widgets;

use yii\base\Widget;
use common\models\website\BannerLang;
use common\models\website\Bannertype;
use yii\helpers\ArrayHelper;

class MainSlider extends Widget {

    public function run() {

        $bannerType = Bannertype::findOne([
            'website_id' => \Yii::$app->urlManager->webSite->id,
            'type' => 'slider',
            'status' => 1
        ]);
        if (is_null($bannerType)) {
            \Yii::error('Слайдер главной страницы не найден');
            return '';
        }

        $slides = $bannerType->getBanners()
                ->andWhere([
                    'status' => 1,
                ])
                ->orderBy(['sort' => SORT_ASC])
                ->all();
        if (empty($slides)) {
            return '';
        }
        $slidesLang = BannerLang::find()
                ->where([
                    'banner_id' => ArrayHelper::map($slides, 'id', 'id'),
                    'lang' => \Yii::$app->language,
                ])
                ->indexBy('banner_id')
                ->all();

        return $this->render('main_slider', [
            'slides' => $slides,
            'slidesLang' => $slidesLang,
        ]);
    }

}
