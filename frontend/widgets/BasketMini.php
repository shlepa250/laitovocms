<?php
namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Json;

class BasketMini extends Widget {

    /**
     * @var \frontend\components\Basket корзина
     */
    public $basket = null;

    public function init() {
        if (is_null($this->basket)) {
            $this->basket = \frontend\components\Basket::getLast();
        }
    }

    private function _getImage(&$catalogProduct, &$catalogProductsLang) {
        $image = $catalogProductsLang[$catalogProduct->id]->image;

        if (empty($image)) {
            $json = Json::decode($catalogProduct->json);
            if (isset($json['photos']) && empty($json['photos'][0]) === false) {
                $image = array_shift($json['photos']);
            }
        }

        return $image;
    }

    public function run() {
        $count = $price = 0;

        $items = $images = $catalogProducts = [];
        if (is_null($this->basket) === false) {
            $count = $this->basket->getTotalCount();
            $price = $this->basket->getTotalPrice();

            $items = $this->basket->getItems();
            $catalogProducts = \common\models\website\CatalogProduct::find()
                    ->indexBy('product_id')
                    ->where(['product_id' => array_keys($items)])
                    ->all();
            $catalogProductsLang = \common\models\website\CatalogProductLang::find()
                    ->indexBy('catalog_product_id')
                    ->where([
                        'catalog_product_id' => \yii\helpers\ArrayHelper::map($catalogProducts, 'id', 'id'),
                        'lang' => \Yii::$app->language,
                    ])
                    ->all();
            foreach ($catalogProducts as $catalogProduct) {
                $images[$catalogProduct->product_id] = $this->_getImage($catalogProduct, $catalogProductsLang);
            }
        }

        return $this->render('basket_mini', [
            'basket' => $this->basket,
            'count' => $count,
            'price' => $price,
            'items' => $items,
            'images' => $images,
            'catalogProducts' => $catalogProducts,
        ]);
    }

}
