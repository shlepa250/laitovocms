<?php

namespace frontend\widgets;

use yii\base\Widget;
use common\models\website\CatalogProduct;
use common\models\website\CatalogProductLang;
use common\models\order\Price;
use yii\helpers\Url;
use yii\helpers\Json;
use frontend\components\GeoTool;

class Product extends Widget {

    public $catalogProduct = null;
    public $catalogProductId = null;
    public $catalogProductLang = null;
    
    public $productId = null;
    public $product = null;

    public $price = null;


    private function _getProductInfo() {
        if (is_null($this->productId) === false && is_null($this->product)) {
            $this->product = \common\models\order\Product::findOne([
                'id' => $this->productId,
                'status' => 1,
            ]);
        } elseif(is_null($this->product)) {
            $this->product = \common\models\order\Product::findOne([
                'id' => $this->catalogProduct->product_id,
                'status' => 1,
            ]);
        }

        if (is_null($this->product) || $this->product instanceof \common\models\order\Product === false) {
            throw new \yii\web\ServerErrorHttpException('Product not set');
        }

        $this->productId = $this->product->id;
    }

    private function _getCatalogProductInfo() {
        if (is_null($this->catalogProductId) === false && is_null($this->catalogProduct)) {
            $this->catalogProduct = CatalogProduct::findOne([
                'id' => $this->catalogProductId,
                'status' => 1
            ]);
        } elseif(is_null($this->catalogProduct)) {
            $this->catalogProduct = CatalogProduct::findOne([
                'product_id' => $this->productId,
                'status' => 1
            ]);
        }

        if (is_null($this->catalogProduct) || $this->catalogProduct instanceof CatalogProduct === false) {
            throw new \yii\web\ServerErrorHttpException('Catalog product not set');
        }

        $this->catalogProductId = $this->catalogProduct->id;
        
        if (is_null($this->catalogProductLang)) {
            $this->catalogProductLang = CatalogProductLang::findOne([
                'catalog_product_id' => $this->catalogProductId,
                'lang' => \Yii::$app->language,
            ]);
        }
    }

    private function _getPrice() {
        if (is_null($this->price)) {
            $city = GeoTool::getUserCity();
            $this->price = Price::findOne([
                'region_id' => $city['regions'],
                'product_id' => $this->productId,
            ]);
        }

        if (is_null($this->price)) {
            $this->price = false;
            return;
        }
    }


    public function init() {
        if (is_null($this->catalogProductId) === false || is_null($this->catalogProduct)) {
            $this->_getProductInfo();
            $this->_getCatalogProductInfo();
        } else {
            $this->_getCatalogProductInfo();
            $this->_getProductInfo();
        }

        $this->_getPrice();
    }

    private function _getImage() {
        $image = $this->catalogProductLang->image;
        
        if (empty($image)) {
            $json = Json::decode($this->catalogProduct->json);
            if (isset($json['photos']) && empty($json['photos'][0]) === false) {
                $image = array_shift($json['photos']);
            }
        }
        
        return $image;
    }

    public function run() {
        if ($this->price === false || $this->product === false || $this->catalogProduct === false || $this->catalogProductLang === false) {
            return false;
        }

        $url = Url::to(['site/product', 'id' => $this->catalogProductId]);
        
        return $this->render('product', [
            'product' => $this->catalogProduct,
            'productLang' => $this->catalogProductLang,
            'url' => $url,
            'image' => $this->_getImage(),
            'price' => $this->price,
        ]);
    }

}
