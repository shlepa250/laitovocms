<?php
namespace frontend\widgets;

use yii\base\Widget;
use common\models\order\Product;

class ProductPopularMain extends Widget {

    public function run() {

        $products = Product::find()
                ->indexBy('id')
                ->where(['<', 'id', 20])
                ->orderBy(new \yii\db\Expression('RAND()'))
                ->limit(12)
                ->all();



        return $this->render('product_popular_main', [
            'products' => $products,
        ]);
    }

}
