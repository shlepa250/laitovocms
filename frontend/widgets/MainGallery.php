<?php

namespace frontend\widgets;

use yii\base\Widget;
use common\models\website\Gallery;
use common\models\website\GalleryLang;
use common\models\website\Photo;
use common\models\website\PhotoLang;
use yii\helpers\ArrayHelper;

class MainGallery extends Widget {

    public function run() {

        $gallerys = Gallery::find()
                ->where([
                    'website_id' => \Yii::$app->urlManager->webSite->id,
                    'status' => 1,
                ])
                ->orderBy(['sort' => SORT_ASC])
                ->all();
        if (empty($gallerys)) {
            return '';
        }
        $galleryIds = ArrayHelper::map($gallerys, 'id', 'id');
        $gallerysLang = GalleryLang::find()
                ->where([
                    'gallery_id' => $galleryIds,
                    'lang' => \Yii::$app->language,
                ])
                ->indexBy('gallery_id')
                ->all();
        
        $photos = Photo::find()
                ->where([
                    'gallery_id' => $galleryIds,
                    'status' => 1,
                ])
                ->orderBy(['sort' => SORT_ASC])
                ->all();

        $photosGallery = [];
        foreach ($photos as $photo) {
            $photosGallery[$photo->gallery_id][] = $photo;
        }

        $photoLang = PhotoLang::find()
                ->where([
                    'photo_id' => ArrayHelper::map($photos, 'id', 'id'),
                    'lang' => \Yii::$app->language,
                ])
                ->indexBy('photo_id')
                ->all();
        
        return $this->render('main_gallery', [
            'gallerys' => $gallerys,
            'galleryLang' => $gallerysLang,
            'photos' => $photosGallery,
            'photoLang' => $photoLang,
        ]);
    }

}
