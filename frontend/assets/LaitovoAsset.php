<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class LaitovoAsset extends AssetBundle
{
    public $baseUrl = '@frontend/themes/laitovo/assets';
    public $sourcePath = '@frontend/themes/laitovo/assets';
    
    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:400,400italic,300italic,300,500,500italic,700,700italic&amp;subset=latin,cyrillic-ext,cyrillic',
        'css/style.css',
    ];
    public $js = [
        'js/jquery.main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'frontend\assets\ShopAsset',
    ];
    
    public static function imgSrc($relativePath = '', $assetImageDir = 'img') {
        $obj = new self();
        return \Yii::$app->assetManager->getPublishedUrl($obj->sourcePath)
                . "/" . $assetImageDir . "/" . $relativePath;
    }

    public function init() {
        parent::init();

        if (YII_DEBUG && !\Yii::$app->request->isPjax) {
            $this->publishOptions['forceCopy'] = true;
        }
        $this->addIeStyle();
    }

    public function addIeStyle() {
        $view = \Yii::$app->getView();
        $manager = $view->getAssetManager();
        $publishUrl = $manager->getPublishedUrl($this->sourcePath);
        $view->registerJsFile($publishUrl . '/js/ie_v678.js', ['condition' => 'lte IE 8']);
        $view->registerCssFile($publishUrl . '/css/ie.css', ['condition' => 'lte IE 8']);
    }
}
