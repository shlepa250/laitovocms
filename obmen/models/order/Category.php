<?php

namespace obmen\models\order;

use Yii;
use yii\helpers\Json;
use obmen\models\team\Team;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Category extends \common\models\order\Category
{
    /**
     * @ignore
     */
    public function rules()
    {
        return [
            [['team_id'], 'required'],
            [['team_id','parent_id'], 'number'],
            [['name'], 'string', 'max'=>255],
            [['json'], 'string'],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields['json']=function () {
            return Json::decode($this->json);
        };

        $fields['created_at']=function () {
            return $this->created_at ? Yii::$app->formatter->asDatetime($this->created_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        $fields['updated_at']=function () {
            return $this->updated_at ? Yii::$app->formatter->asDatetime($this->updated_at,'dd.MM.yyyy HH:mm:ss') : null;
        };

        return $fields;
    }

}
