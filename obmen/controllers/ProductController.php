<?php
namespace obmen\controllers;

use Yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use obmen\models\order\Category;

/**
 * Product controller
 */
class ProductController extends Controller
{

    public $modelClass = 'obmen\models\order\Product';

    public function behaviors()
    {

        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex($team, $last=null)
    {
        $modelClass = $this->modelClass;
        
        $query = $modelClass::find()->where(['team_id'=>$team]);

        if ($last) $query->andWhere(['>','updated_at',time()-$last]);

        return new ActiveDataProvider([
            'pagination' => [
                'pageSize' => 100000,
            ],
            'query' => $query,
        ]);
    }

    public function actionCategory($team)
    {
        
        return new ActiveDataProvider([
            'pagination' => [
                'pageSize' => 100000,
            ],
            'query' => Category::find()->where(['team_id'=>$team]),
        ]);
    }

}
