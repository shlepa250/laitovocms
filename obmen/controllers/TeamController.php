<?php
namespace obmen\controllers;

use Yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;

/**
 * Team controller
 */
class TeamController extends Controller
{

    public $modelClass = 'obmen\models\team\Team';

    public function behaviors()
    {

        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex($team, $last=null)
    {
        $modelClass = $this->modelClass;
        
        $query = $modelClass::find()->select('clients.*')->joinWith('clients clients')->where(['team_id'=>$team]);

        if ($last) $query->andWhere(['>','clients.updated_at',time()-$last]);

        return new ActiveDataProvider([
            'pagination' => [
                'pageSize' => 100000,
            ],
            'query' => $query,
        ]);
    }

}
